using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    /// <summary>
    /// 플레이어 UI 패널
    /// </summary>
    public PlayerUIPanel playerUIPanel { get; private set; }

    public EnemyHpUI enemyHpUI { get; private set; }

    private void Awake()
    {
        playerUIPanel = GetComponentInChildren<PlayerUIPanel>();

        enemyHpUI = GetComponentInChildren<EnemyHpUI>();
    }

}
