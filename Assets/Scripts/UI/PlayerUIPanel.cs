using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIPanel : MonoBehaviour
{
    /// <summary>
    /// 플레이어 상태 패널
    /// </summary>
    public PlayerStatePanel playerStatePanel { get; private set; }

    private void Awake()
    {
        playerStatePanel = GetComponentInChildren<PlayerStatePanel>();
    }

}
