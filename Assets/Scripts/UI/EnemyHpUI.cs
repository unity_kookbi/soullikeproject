using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 적 HP UI 를 나타내기 위한 컴포넌트입니다.
/// </summary>
public sealed class EnemyHpUI : MonoBehaviour
{
    [Header("# 이름")]
    public TMP_Text m_NameText;

    [Header("# Hpbar")]
    public Image m_HpbarImage;

    /// <summary>
    /// UI 에 표시할
    /// </summary>
    private EnemyCharacter _TargetEnemy;

    /// <summary>
    /// 표시중인 적의 최대 체력을 나타냅니다.
    /// </summary>
    private float _MaxHp;

    /// <summary>
    /// 표시된 시간
    /// </summary>
    private float _StartVisibleTimeSeconds;


    private void Start()
    {
        // 처음에는 UI 가 표시되지 않도록 합니다.
        gameObject.SetActive(false);
    }

    private void Update()
    {
        // 표시 시간이 지난 경우 비활성화 시킵니다.
        if(Time.time > _StartVisibleTimeSeconds + Constants.ENEMY_STATEUI_VISIBLE_TIMESECONDS)
        {
            gameObject.SetActive(false);
        }

        if(_TargetEnemy) UpdateHpbar();
        else 
        {
            _TargetEnemy = null;
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// UI 에 표시할 적 캐릭터를 설정합니다.
    /// </summary>
    /// <param name="newTargetEnemy"></param>
    public void SetTargetEnemy(EnemyCharacter newTargetEnemy)
    {
        // 표시된 시간을 기록합니다
        _StartVisibleTimeSeconds = Time.time;

        // UI 를 표시힙나디
        gameObject.SetActive(true);

        _TargetEnemy = newTargetEnemy;

        m_NameText.text = newTargetEnemy.enemyData.enemyName;

        _MaxHp = newTargetEnemy.enemyData.maxHp;


        // 체력바 갱신
        UpdateHpbar();

    }

    /// <summary>
    /// 체력바를 갱신합니다.
    /// </summary>
    private void UpdateHpbar()
    {
        m_HpbarImage.fillAmount = _TargetEnemy.currentHp / _MaxHp; 
    }


}
