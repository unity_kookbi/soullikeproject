using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class WolfAttack : MonoBehaviour
{
    [Header("# 공격")]
    public Transform m_AttackCheckStartTransform;
    public float m_AttackCheckMaxDistance;

    private WolfCharacter _WolfCharacter;

    /// <summary>
    /// 공격중 상태를 나타냅니다.
    /// </summary>
    public bool isAttacking { get; private set; }

    #region DEBUG
    private DrawGizmoSphereInfo _DebugDrawGizmoAttackArea;
    #endregion

    private void Awake()
    {
        _WolfCharacter = GetComponent<WolfCharacter>();
    }

    /// <summary>
    /// 공격이 시작되었을 때 호출됩니다.
    /// </summary>
    public void OnAttackStarted()
    {
        isAttacking = true;

        // 공격 애니메이션 재생
        (_WolfCharacter.animController as WolfAnimController).PlayAttackAnimation();

        //// 플레이어 방향으로 회전합니다.
        //PlayerCharacter playerCharacter = _WolfCharacter.behaviorController.GetKeyAsObject<PlayerCharacter>(
        //    BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER);
        //
        //if(playerCharacter != null)
        //{
        //    _WolfCharacter.LookAt(playerCharacter.gameObject);
        //}
    }

    /// <summary>
    /// 공격이 끝났을 때 호출됩니다.
    /// </summary>
    public void OnAttackFinished()
    {
        if (isAttacking)
        {
            isAttacking = false;
        }
    }


    /// <summary>
    /// 공격 영역을 확인합니다.
    /// </summary>
    public void CheckAttackArea()
    {
        Vector3 start = m_AttackCheckStartTransform.position;
        Ray ray = new Ray(start, transform.forward);

        Collider[] detectedCollisions = PhysicsExt.OverlapSphere(
            out _DebugDrawGizmoAttackArea,
            start,
            m_AttackCheckMaxDistance,
            Constants.LAYER_PLAYERCHARACTER);

        bool isHit = detectedCollisions.Length > 0;
        GameObject detectedGameObject = isHit ? detectedCollisions[0].gameObject : null;

        if (!isHit)
        {
            isHit = PhysicsExt.SphereCast(
                out _DebugDrawGizmoAttackArea,
                ray, m_AttackCheckMaxDistance,
                out RaycastHit hitResult,
                m_AttackCheckMaxDistance,
                Constants.LAYER_PLAYERCHARACTER);

            if(isHit) detectedGameObject = hitResult.transform.gameObject;
        }


        if (isHit)
        {
            // 플레이어에게 피해를 입힙니다.
            _WolfCharacter.ApplyDamageToPlayerCharacter(detectedGameObject, _WolfCharacter.enemyData.atk);
        }
    }



#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (isAttacking)
        {
            PhysicsExt.DrawGizmoSphere(_DebugDrawGizmoAttackArea);
        }
    }

#endif
}
