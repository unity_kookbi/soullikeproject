using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfCharacter : EnemyCharacter
{
    [Header("# Body Collisions")]
    public List<Collider> m_BodyCollisions;

    /// <summary>
    /// 그룹에 속한 Wolf Character 객체를 나타냅니다.
    /// </summary>
    private List<WolfCharacter> _WolfGroup;

    private WolfAttack _WolfAttack;

    public WolfAttack wolfAttack => _WolfAttack ?? ( _WolfAttack = GetComponent<WolfAttack>());

    public WolfAnimController wolfAnimController => animController as WolfAnimController;

    protected override void Awake()
    {
        base.Awake();

        // 최대 속력 지정
        agent.speed = 6.0f;
    }

    protected override void Start()
    {
        base.Start();

        // 그룹에 등록시킬 Wolf Character 들을 모두 찾습니다.
        FindWolves();

        // 애니메이션 이벤트 바인딩
        wolfAnimController.onAttackFinished += wolfAttack.OnAttackFinished;
        wolfAnimController.onAttack += wolfAttack.CheckAttackArea;
    }

    protected override void Update()
    {
        base.Update();

        WolfAnimController wolfAnimController = animController as WolfAnimController;
        
        // 현재 속력 갱신
        wolfAnimController.currentSpeed = agent.velocity.magnitude;
    }

    /// <summary>
    /// 자신을 제외한 WolfCharacter 들을 찾습니다.
    /// </summary>
    private void FindWolves()
    {
        _WolfGroup = new();
        foreach(Collider collision in Physics.OverlapSphere(transform.position,5.0f, 1 << gameObject.layer))
        {
            WolfCharacter wolfCharacter = collision.GetComponent<WolfCharacter>();

            if(wolfCharacter && wolfCharacter != this) _WolfGroup.Add(wolfCharacter);
        }
    }

    public override void OnApplyDamage(GameObject damageCauser, float damage)
    {
        base.OnApplyDamage(damageCauser, damage);

        // 무리에 속한 WolfCharacter 들이 같이 공격적인 상태가 되도록합니다.
        foreach(WolfCharacter wolfCharacter in _WolfGroup) 
        {
            if(wolfCharacter.isDead) continue;

            wolfCharacter.behaviorController.SetKey(
                BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER, damageCauser.GetComponent<PlayerCharacter>());

            wolfCharacter.behaviorController.SetKey(
                BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE, true);

            // 행동을 재실행합니다.
            wolfCharacter.behaviorController.StopBehavior();
            (wolfCharacter.behaviorController as BehaviorController_Enemy).StartBehavior();
        }
    }

    public override void OnKnockback(float power, Vector3 direction, System.Action onKnockbackFinished = null)
    {
        base.OnKnockback(power,direction, onKnockbackFinished);

        // 행동을 중단시킵니다.
        behaviorController.StopBehavior();

        // Hit 애니메이션 재생
        (animController as WolfAnimController).StartHitAnimation();
    }

    protected override IEnumerator OnKnockbackFinished(Action onKnockbackFinished)
    {
        yield return base.OnKnockbackFinished(onKnockbackFinished);

        // 공격 상대값 초기화
        wolfAttack.OnAttackFinished(); 
        behaviorController.SetKey(BehaviorController_Wolf.KEYNAME_ATTACKREQUESTED, false);

        // 행동 재실행
        (behaviorController as BehaviorController_Wolf).StartBehavior();

        if(_WolfAttack.isAttacking)
        {
            _WolfAttack.OnAttackFinished();
        }
    }

    public override void OnDead()
    {
        base.OnDead();

        // 적 캐릭터의 충돌을 위해 사용되는 컬리전을 모두 비활성화 시킵니다.
        foreach(Collider collision in m_BodyCollisions) 
        {
            collision.enabled = false;
        }
    }
}
