using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfAnimController : AnimController
{
    private const string PARAMNAME_SPEED = "_Speed";

    private const string PARAMNAME_ATTACK = "_Attack";

    private const string PARAMNAME_HIT = "_Hit";

    /// <summary>
    /// 공격 끝 이벤트
    /// </summary>
    public event System.Action onAttackFinished;

    /// <summary>
    /// 공격 이벤트
    /// </summary>
    public event System.Action onAttack;

    /// <summary>
    /// 현재 속력입니다.
    /// </summary>
    public float currentSpeed { get; set; }

    private void Update()
    {
        SetFloat(PARAMNAME_SPEED, currentSpeed);
    }

    public void PlayAttackAnimation()
    {
        SetTrigger(PARAMNAME_ATTACK);
    }

    /// <summary>
    /// Hit 애니메이션을 재생시킵니다.
    /// </summary>
    public void StartHitAnimation()
    {
        SetTrigger(PARAMNAME_HIT);
    }

    /// <summary>
    /// 공격 애니메이션이 끝났을 때 호출됩니다.
    /// </summary>
    public void AnimEvent_AttackFinished() => onAttackFinished?.Invoke();

    /// <summary>
    /// 공격 타이밍에 호출됩니다.
    /// </summary>
    public void AnimEvent_Attack() =>onAttack?.Invoke();


}
