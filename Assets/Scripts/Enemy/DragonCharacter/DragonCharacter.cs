using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonCharacter : EnemyCharacter
{
    /// <summary>
    /// Second Phase 로 넘어가는 구간의 체력 설정
    /// </summary>
    public const float SECOND_PHASE_HP = 800.0f;

    /// <summary>
    /// 가까운 거리로 인식하게 될 최대 거리
    /// </summary>
    public const float NEARDISTANCE = 15.0f;

    public DragonMovement movement { get; private set; }

    public DragonAttack attack { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        // Find Component
        movement = GetComponent<DragonMovement>();
        attack = GetComponent<DragonAttack>();

        // Bind Events
        DragonAnimController animController = this.animController as DragonAnimController;
        animController.onTurnStarted += movement.StartYawTurning;
        animController.onDashBackwardStarted += movement.StartDashBackward;
        animController.onDashBackwardFinished += movement.FinishDashBackward;
        animController.onSpreadFireStarted += attack.OnSpreadFireStarted;
        animController.onSpreadFireFinished += attack.OnSpreadFireFinished;
        animController.onDoubleClawAttackStarted += attack.OnClawAttackStarted;
        animController.onDoubleClawAttackFinished += attack.OnClawAttackFinished;
        animController.onDashForward += movement.DashForward;
    }

    protected override void Update()
    {
        base.Update();

        DragonAnimController animController = this.animController as DragonAnimController;

        UpdateDistanceToPlayer();

        animController.UpdateSpeed(agent.velocity.sqrMagnitude < 0.1f ? 0.0f : agent.speed);
    }


    /// <summary>
    /// 플레이어와 거리를 갱신합니다.
    /// </summary>
    private void UpdateDistanceToPlayer()
    {
        // 플레이어 캐릭터 객체를 얻습니다.
        PlayerCharacter playerCharacter = behaviorController.GetKeyAsObject<PlayerCharacter>(
            BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER);

        // 플레이어 캐릭터가 유효하지 않은 경우 함수 호출 종료
        if (playerCharacter == null) return;

        // 둘사이의 거리를 얻습니다.
        float distance = Vector3.Distance(playerCharacter.transform.position , transform.position);

        // 계산된 거리를 설정합니다.
        behaviorController.SetKey(BehaviorController_Dragon.KEYNAME_DISTANCETOPLAYER, distance);
    }

}
