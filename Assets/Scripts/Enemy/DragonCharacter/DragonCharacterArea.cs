using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public sealed class DragonCharacterArea : MonoBehaviour
{
    public BehaviorController_Enemy m_EnemyBehaviorController;

    private void Awake()
    {
        GetComponent<SphereCollider>().isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        // 플레이어를 감지한 경우
        if(other.tag == Constants.TAG_PLAYER)
        {
            PlayerCharacter playerCharacter = other.GetComponent<PlayerCharacter>();

            if (playerCharacter == null) return;

            DragonCharacter dragonCharacter = m_EnemyBehaviorController.GetComponent<DragonCharacter>();

            SphereCollider area = GetComponent<SphereCollider>();

            // 영역을 설정합니다.
            dragonCharacter.movement.SetMovableArea(area.transform.position + area.center, area.radius);

            // 플레이어 캐릭터를 설정합니다.
            m_EnemyBehaviorController.SetKey(
                BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER, playerCharacter);

            // 행동 실행
            m_EnemyBehaviorController.StartBehavior();

            // 영역 오브젝트 제거
            Destroy(gameObject);
        }
    }
}
