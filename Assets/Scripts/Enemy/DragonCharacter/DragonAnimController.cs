using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAnimController : AnimController
{
    private const string PARAMNAME_TURNLEFT = "_TurnLeft";

    private const string PARAMNAME_TURNRIGHT = "_TurnRight";

    private const string PARAMNAME_DASHBACKWARD = "_DashBackward";

    private const string PARAMNAME_SPREADFIRE = "_SpreadFire";

    private const string PARAMNAME_DOUBLECLAWATTACK = "_DoubleClawAttack";

    private const string PARAMNAME_SPEED = "_Speed";

    /// <summary>
    /// 회전 시작시 발생하는 이벤트입니다.
    /// </summary>
    public event System.Action onTurnStarted;

    /// <summary>
    /// DashBackward 시 발생하는 이벤트
    /// </summary>
    public event System.Action onDashBackwardStarted;
    /// <summary>
    /// DashBackward 종료시 발생하는 이벤트
    /// </summary>
    public event System.Action onDashBackwardFinished;

    public event System.Action onSpreadFireStarted;
    public event System.Action onSpreadFireFinished;

    public event System.Action onDoubleClawAttackStarted;
    public event System.Action onDoubleClawAttackFinished;

    public event System.Action<float> onDashForward;

    public void PlayTurnLeftAnimation() => SetTrigger(PARAMNAME_TURNLEFT);
    public void PlayTurnRightAnimation() => SetTrigger(PARAMNAME_TURNRIGHT);

    public void PlayDashBackwardAnimation() => SetTrigger(PARAMNAME_DASHBACKWARD);

    public void PlaySpreadFireAnimation() => SetTrigger(PARAMNAME_SPREADFIRE);

    public void PlayDoubleClawAttackAnimation() => SetTrigger(PARAMNAME_DOUBLECLAWATTACK);

    public void UpdateSpeed(float speed) => SetFloat(PARAMNAME_SPEED, speed);

    private void AnimEvent_TurnStart() => onTurnStarted?.Invoke();

    private void AnimEvent_DashBackStart() => onDashBackwardStarted?.Invoke();
    private void AnimEvent_DashBackFinish() => onDashBackwardFinished?.Invoke();

    private void AnimEvent_SpreadFireStart() => onSpreadFireStarted?.Invoke();
    private void AnimEvent_SpreadFireFinish() => onSpreadFireFinished?.Invoke();

    private void AnimEvent_DoubleClawAttackStart() => onDoubleClawAttackStarted?.Invoke();
    private void AnimEvent_DoubleClawAttackFinish() => onDoubleClawAttackFinished?.Invoke();

    private void AnimEvent_OnDashForward(float power) => onDashForward?.Invoke(power);
}
