using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class EnemyCharacter : MonoBehaviour, IDamageInterface 
{
    /// <summary>
    /// 적 정보를 담는 ScriptableObject 입니다.
    /// </summary>
    protected static EnemyDataScriptableObject m_EnemyDataScriptableObject;


    [Header("# 적 코드")]
    public string m_EnemyCode;

    [Header("# 이동 관련")]
    public float m_MaxSpeed = 3.5f;

    [Header("# 위치 체크용 바닥 레이어")]
    public LayerMask m_FloorLayer;

    [Header("# Check PlayerCharacter")]
    public float m_CheckRadius = 0.5f;
    public float m_CheckMaxDistance = 1.0f;
    public float m_CheckOffsetY = 1.0f;
    public LayerMask m_PlayerCharacterLayer;


    /// <summary>
    /// 랙돌을 위한 Rigidbody 컴포넌트들 입니다.
    /// </summary>
    private List<Rigidbody> _RagdollRigidBodies;

    /// <summary>
    /// 넉백이 끝날 때까지 대기하는 객체입니다.
    /// </summary>
    private WaitForSeconds _WaitKnockbackFinish;

    /// <summary>
    /// 넉백 끝 대기 루틴
    /// </summary>
    private Coroutine _WaitKnockbackFinishRoutine;

    /// <summary>
    /// 적 정보에 대한 프로퍼티입니다.
    /// </summary>
    public EnemyData enemyData { get; private set; }

    /// <summary>
    /// 행동 제어 컴포넌트에 대한 프로퍼티입니다.
    /// </summary>
    public BehaviorController_Enemy behaviorController { get; protected set; }

    /// <summary>
    /// 길찾기를 위한 NavMeshAgent 컴포넌트 프로퍼티입니다.
    /// </summary>
    public NavMeshAgent agent { get; private set; }

    /// <summary>
    /// 오브젝트 하위에 추가되는 AnimController 컴포넌트 프로퍼티입니다.
    /// </summary>
    public AnimController animController { get; private set; }

    public CapsuleCollider bodyCollision { get; private set; }

    /// <summary>
    /// 현재 체력을 나타냅니다.
    /// </summary>
    public float currentHp { get; protected set; }

    /// <summary>
    /// 사망 상태를 나타냅니다.
    /// </summary>
    public bool isDead { get; protected set; }

    /// <summary>
    /// 넉백중임을 나타냅니다.
    /// </summary>
    public bool isKnockback { get; protected set; }

    #region DEBUG
    private DrawGizmoLineInfo _FloorCheckDrawGizmoLineInfo;
    private DrawGizmoSphereInfo _CheckPlayerCharacterGizmoInfo;
    #endregion

    protected virtual void Awake()
    {
        // 적 정보를 담는 ScriptableObject 에셋 로드
        if (m_EnemyDataScriptableObject == null)
        {
            m_EnemyDataScriptableObject = Resources.Load<EnemyDataScriptableObject>(
                "ScriptableObject/EnemyData");
        }

        // 적 정보 초기화
        enemyData = m_EnemyDataScriptableObject.GetEnemyData(m_EnemyCode);

        // 체력 초기화
        currentHp = enemyData.maxHp;

        behaviorController = GetComponent<BehaviorController_Enemy>();

        animController = GetComponentInChildren<AnimController>();

        bodyCollision = GetComponent<CapsuleCollider>();

        agent = GetComponent<NavMeshAgent>();

        // 넉백 끝 대기 객체 초기화
        _WaitKnockbackFinish = new WaitForSeconds(0.7f);

        // 랙돌 초기화
        InitializeRagdoll();
    }

    protected virtual void Start() { }

    protected virtual void FixedUpdate()
    {
        if(isDead) return;
        if (isKnockback) return;

        if(CheckPlayerCharacter())
        {
            agent.SetDestination(transform.position);
        }
        else
        {
            agent.speed = m_MaxSpeed;
        }
    }

    protected virtual void Update()
    {
        if (isKnockback) return;

        // Agent 멈춤 설정
        UpdateNavMeshAgentStopState();

    }

    private DrawGizmoSphereInfo _DebugDrawGizmoKnockback;


    /// <summary>
    /// NavMeshAgent 의 멈춤 상태를 갱신합니다.
    /// </summary>
    private void UpdateNavMeshAgentStopState()
    {
        if(isDead) return;

        Ray ray = new Ray(transform.position + Vector3.up, Vector3.down);

        bool isHit = PhysicsExt.Raycast(
            out _FloorCheckDrawGizmoLineInfo,
            ray,
            out RaycastHit hitResult,
            Mathf.Infinity,
            m_FloorLayer);

        if(isHit) 
        {
            // 목적지 위치에 도달한 경우
            bool isGoal = Vector3.Distance(agent.destination, hitResult.point) <= agent.stoppingDistance;

            // 현재 속력이 0인지 확인합니다.
            bool isStopped = agent.velocity.sqrMagnitude == 0.0f;

            // 두 조건을 만족하는 경우 멈춤 상태로 설정합니다.
            agent.isStopped = isGoal && isStopped;
        }
    }

    /// <summary>
    /// 이동 방향에 플레이어 캐릭터가 존재하는지 확인합니다.
    /// </summary>
    private bool CheckPlayerCharacter()
    {
        // 이동하지 않는 경우에는 확인하지 않습니다.
        if (agent.velocity.sqrMagnitude == 0.0f) return false;

        // 이동 방향을 얻습니다.
        Vector3 moveDirection = agent.velocity;
        moveDirection.Normalize();

        Ray ray = new Ray(transform.position + Vector3.up * m_CheckOffsetY, moveDirection);
        bool isHit = PhysicsExt.SphereCast(
            out _CheckPlayerCharacterGizmoInfo,
            ray, 
            m_CheckRadius,
            out RaycastHit hitResult,
            m_CheckMaxDistance,
            m_PlayerCharacterLayer, 
            QueryTriggerInteraction.UseGlobal);

        return isHit;
    }


    /// <summary>
    /// 피해를 입을 경우 호출됩니다.
    /// </summary>
    /// <param name="damageCauser">피해를 입힌 캐릭터가 전달됩니다.</param>
    /// <param name="damage">받은 피해량이 전달됩니다.</param>
    public virtual void OnApplyDamage(GameObject damageCauser, float damage)
    {
        // HP 계산
        currentHp -= damage;

        // 이전 상태를 얻습니다.
        bool prevAggressiveState = behaviorController.GetKeyAsValue<bool>(
            BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE);

        // 피해를 입힌 플레이어 캐릭터 객체를 얻습니다.
        PlayerCharacter playerCharacter = damageCauser.GetComponent<PlayerCharacter>();

        Vector3 playerPosition = playerCharacter.transform.position;
        playerPosition.y = 0.0f;

        Vector3 currentPosition = transform.position;
        currentPosition.y = 0.0f;

        // 플레이어 -> 적 방향을 계산합니다.
        Vector3 direction =  (currentPosition - playerPosition).normalized;

        OnKnockback(5.0f, direction);

        // 공격적인 상태가 아니라면
        if (!prevAggressiveState) 
        {
            // 공격적인 상태로 변경합니다.
            behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE, true);

            // 피해를 입힌 객체 (플레이어 캐릭터)를 설정합니다.
            behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER, playerCharacter);

            behaviorController.StopBehavior();
            (behaviorController as BehaviorController_Enemy).StartBehavior();
        }

        if(currentHp < 0.0f)
        {
            currentHp = 0.0f;

            // 사망 처리
            OnDead();
        }
    }

    /// <summary>
    /// 랙돌 초기화
    /// </summary>
    public virtual void InitializeRagdoll()
    {
        _RagdollRigidBodies = new(GetComponentsInChildren<Rigidbody>());

        foreach(Rigidbody rigidbody in _RagdollRigidBodies)
        {
            // 레이어를 설정합니다.
            rigidbody.gameObject.layer = LayerMask.NameToLayer("Ragdoll");

            // 다이나믹 충돌 검사를 진행하도록 합니다.
            rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

            // 중력의 영향을 받지 않도록 합니다.
            rigidbody.useGravity = false;

            // 물리엔진으로 해당 오브젝트가 제어되지 않도록 설정합니다.
            rigidbody.isKinematic = true;
        }
    }

    /// <summary>
    /// 플레이어 캐릭터 방향으로 회전합니다.
    /// </summary>
    /// <param name="targetObject">목표 오브젝트 위치를 전달합니다.</param>
    public virtual void LookAt(GameObject targetObject)
    {
        // 목표 오브젝트의 위치를 얻습니다.
        Vector3 targetPosition = targetObject.transform.position;

        targetPosition.y = 0.0f;

        // 현재 위치를 얻습니다.
        Vector3 currentPosition = transform.position;
        currentPosition.y = 0.0f;

        // 목표로 향하는 방향을 얻습니다.
        Vector3 direction = (targetPosition - currentPosition).normalized;

        // Yaw 회전 각도를 얻습니다.
        float yawAngle = Mathf.Atan2(
            direction.x, direction.z) * Mathf.Rad2Deg;

        // 회전값을 적용합니다.
        transform.rotation = Quaternion.Euler(0.0f, yawAngle, 0.0f);

    }

    /// <summary>
    /// 플레이어에게 피해를 가합니다.
    /// </summary>
    /// <param name="detectedObject">감지된 오브젝트를 전달합니다.</param>
    /// <param name="damage">가할 피해를 전달합니다.</param>
    /// <param name="isBlockable">공격을 플레이어가 막을 수 있는지 제어 가능</param>
    public virtual void ApplyDamageToPlayerCharacter(
        GameObject detectedObject, float damage, bool isBlockable = true)
    {
        // 플레이어 캐릭터 객체를 얻습니다.
        PlayerCharacter playerCharacter = detectedObject.GetComponent<PlayerCharacter>();

        // 감지된 오브젝트가 플레이어 캐릭터가 아닌 경우 함수 호출 종료
        if (playerCharacter == null) return;

        // 실제로 가할 피해량을 계산합니다.
        damage = CalculateDamage(damage);
        
        // 피해량을 전달합니다.
        playerCharacter.OnApplyDamage(gameObject, damage);
    }

    public virtual float CalculateDamage(float originalDamage)
    {
        return originalDamage;
    }

    public virtual void OnKnockback(float power, Vector3 direction, System.Action onKnockbackFinished = null)
    {
        // 이미 진행중인 넉백 대기 루틴이 존재한다면
        if(_WaitKnockbackFinishRoutine != null)
        {
            // 중단합니다.
            StopCoroutine(_WaitKnockbackFinishRoutine);
            _WaitKnockbackFinishRoutine = null;
        }

        // 넉백 상태로 설정합니다.
        isKnockback = true;

        float prevSpeed = agent.speed;
        float prevAngularSpeed = agent.angularSpeed;
        float prevAcceleration = agent.acceleration;

        // NavMeshAgent 에 적용된 속도를 변경합니다.
        agent.SetDestination(transform.position + direction * power);
        agent.velocity += direction * power;


        // 넉백 끝을 대기하기 시작합니다.
        _WaitKnockbackFinishRoutine = StartCoroutine(OnKnockbackFinished(onKnockbackFinished));
    }

    protected virtual IEnumerator OnKnockbackFinished(
        System.Action onKnockbackFinished)
    {
        // 넉백이 끝날 때까지 대기합니다.
        yield return _WaitKnockbackFinish;

        agent.SetDestination(transform.position);

        // 넉백 상태를 비활성화 시킵니다.
        isKnockback = false;

        _WaitKnockbackFinishRoutine = null;
    }

    /// <summary>
    /// 적이 사망할 경우 호출됩니다.
    /// </summary>
    public virtual void OnDead()
    {
        // 사망 상태로 설정합니다.
        isDead = true;

        // 애니메이션 재생 종료
        animController.animator.enabled = false;

        // 행동을 중지
        behaviorController.StopBehavior();

        foreach (Rigidbody rigidbody in _RagdollRigidBodies)
        {
            // 중력의 영향을 받도록 합니다.
            rigidbody.useGravity = true;

            // 물리엔진으로 해당 오브젝트가 제어되도록 설정합니다.
            rigidbody.isKinematic = false;

            rigidbody.AddForce(Random.insideUnitSphere * 300.0f);
        }

        // 더이상 이동하지 않도록 합니다.
        agent.SetDestination(transform.position);

        agent.height = 0.0f;

        // 오브젝트를 5초후에 제거합니다.
        Destroy(gameObject, 5.0f);
    }


#if UNITY_EDITOR
    protected virtual void OnDrawGizmos()
    {
        PhysicsExt.DrawGizmoLine(in _FloorCheckDrawGizmoLineInfo);
        PhysicsExt.DrawGizmoSphere(in _CheckPlayerCharacterGizmoInfo);
    }


#endif
}
