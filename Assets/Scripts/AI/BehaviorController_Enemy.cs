﻿using UnityEngine.AI;

/// <summary>
///  적 객체에서 사용되는 행동 제어 클래스의 기반 형식입니다.
/// </summary>
public abstract class BehaviorController_Enemy : BehaviorController
{
    /// <summary>
    /// 이 행동을 실행하는 적 캐릭터를 나타냅니다.
    /// </summary>
    public const string KEYNAME_OWNERENEMYCHARACTER = "OwnerEnemyCharacter";

    /// <summary>
    /// 생성된 위치를 기록할 키 이름입니다.
    /// 이동 범위를 제한시키기 위하여 사용됩니다.
    /// </summary>
    public const string KEYNAME_SPAWNPOSITION = "SpawnPosition";

    /// <summary>
    /// 이동 목표 위치를 기록할 키 이름입니다.
    /// </summary>
    public const string KEYNAME_MOVETARGETPOSITION = "MoveTargetPosition";

    /// <summary>
    /// 생성된 위치부터 이동 가능한 반경을 나타냅니다.
    /// </summary>
    public const string KEYNAME_MAXMOVEDISTANCE = "MaxMoveDistance";

    /// <summary>
    /// 공격상태를 나타내는 키
    /// </summary>
    public const string KEYNAME_ISAGGRESSIVESTATE = "IsAggressiveState";

    /// <summary>
    /// 플레이어 캐릭터
    /// </summary>
    public const string KEYNAME_PLAYERCHARACTER = "PlayerCharacter";

    /// <summary>
    /// 추적 가능 최대 거리
    /// </summary>
    public const string KEYNAME_MAXTRACKINGDISTANCE = "MaxTrackingDistance";

    /// <summary>
    /// 목표 Yaw 회전값을 나타내기 위한 키
    /// </summary>
    public const string KEYNAME_TARGETROTATIONYAW = "TargetRotationYaw";

    private NavMeshAgent _Agent;

    public NavMeshAgent agent => _Agent ?? (_Agent = GetComponent<NavMeshAgent>());

    protected virtual void Awake()
    {
        // 자신을 추가합니다.
        AddKey(KEYNAME_OWNERENEMYCHARACTER, GetComponent<EnemyCharacter>());

        // 생성 위치 키 추가
        AddKey(KEYNAME_SPAWNPOSITION,transform.position);

        // 이동 목표 위치 키 추가
        AddKey(KEYNAME_MOVETARGETPOSITION);

        // 최대 이동 가능 반경 키 추가
        AddKey(KEYNAME_MAXMOVEDISTANCE);

        // 공격적 상태를 나타내는 키 추가
        AddKey(KEYNAME_ISAGGRESSIVESTATE, false);

        // 플레이어 캐릭터 키 추가
        AddKey(KEYNAME_PLAYERCHARACTER);

        // 최대 추적 거리 키 추가
        AddKey(KEYNAME_MAXTRACKINGDISTANCE);

        // 목표 회전값 키 추가
        AddKey(KEYNAME_TARGETROTATIONYAW, 0.0f);
    }

    protected virtual void Start() { }

    /// <summary>
    /// 적 클래스의 행동 시작 메서드입니다.
    /// </summary>
    public abstract void StartBehavior();

#if UNITY_EDITOR
    protected virtual void OnDrawGizmos()
    {
        if(!UnityEditor.EditorApplication.isPlaying) return;
        if (currentBehavior == null) return;

        // 현재 실행중인 Task/Composite 의 OnDrawGizmos() 호출
        currentBehavior.OnDrawGizmos();

        // 현재 실행중인 Task/Composite 에 추가된 Service 의 OnDrawGizmos() 호출
        if(currentBehavior.behaviorServices != null)
        {
            foreach(BehaviorService service in currentBehavior.behaviorServices)
            {
                service.OnDrawGizmos();
            }
        }

    }
    protected virtual void OnDrawGizmosSelected()
    {
        if (!UnityEditor.EditorApplication.isPlaying) return;
        if (currentBehavior == null) return;

        // 현재 실행중인 Task/Composite 의 OnDrawGizmosSelected() 호출
        currentBehavior.OnDrawGizmosSelected();

        // 현재 실행중인 Task/Composite 에 추가된 Service 의 OnDrawGizmosSelected() 호출
        if (currentBehavior.behaviorServices != null)
        {
            foreach (BehaviorService service in currentBehavior.behaviorServices)
            {
                service.OnDrawGizmosSelected();
            }
        }
    }
#endif
}
