using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_DoubleClawAttack : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        DragonCharacter dragonCharacter =
            behaviorController.GetKeyAsObject<DragonCharacter>(BehaviorController_Enemy.KEYNAME_OWNERENEMYCHARACTER);

        DragonAnimController animController = 
            dragonCharacter.animController as DragonAnimController;

        DragonAttack attack = dragonCharacter.attack;

        animController.PlayDoubleClawAttackAnimation();

        yield return new WaitUntil(() => attack.isClawAttack);
        yield return new WaitWhile(() => attack.isClawAttack);

        isSucceeded = true;
    }

}
