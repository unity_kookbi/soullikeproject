using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_SpreadFire : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        DragonCharacter dragonCharacter = behaviorController.GetKeyAsObject<DragonCharacter>(
            BehaviorController_Enemy.KEYNAME_OWNERENEMYCHARACTER);

        DragonAnimController animController = dragonCharacter.animController as DragonAnimController;

        DragonAttack attack = dragonCharacter.attack;

        // 애니메이션 재생
        animController.PlaySpreadFireAnimation();

        // 공격이 시작될 때까지 대기
        yield return new WaitUntil(() => attack.isSpreadFire);

        // 공격이 끝날 때까지 대기
        yield return new WaitWhile(() => attack.isSpreadFire);

        isSucceeded = true;
    }
}
