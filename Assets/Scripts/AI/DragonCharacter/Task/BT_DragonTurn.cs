using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_DragonTurn : RunnableBehavior
{
    // 이 값 미만이라면 회전시키지 않습니다.
    private const float _TURNANGLEMINYAW = 25.0f;

    /// <summary>
    /// 회전 목표로 사용될 플레이어 캐릭터를 나타내기 위한 객체
    /// </summary>
    private string PlayerCharacterKey;

    public BT_DragonTurn(string playerCharacterKey)
    {
        PlayerCharacterKey = playerCharacterKey;
    }

    public override IEnumerator RunBehavior()
    {
        DragonCharacter ownerCharacter = behaviorController.GetKeyAsObject<EnemyCharacter>(
            BehaviorController_Enemy.KEYNAME_OWNERENEMYCHARACTER) as DragonCharacter;

        PlayerCharacter playerCharacter = behaviorController.GetKeyAsObject<PlayerCharacter>(PlayerCharacterKey);

        // 플레리어 캐릭터 유효성검사
        if(playerCharacter == null)
        {
            isSucceeded = false;
            yield break;
        }

        // 플레이어를 바라보는 방향으로 얻습니다.
        Vector3 directionToPlayer =
            playerCharacter.transform.position - behaviorController.transform.position;
        directionToPlayer.y = 0.0f;
        directionToPlayer.Normalize();

        // 회전각을 얻습니다.
        float yawAngle = Mathf.Atan2(directionToPlayer.x,directionToPlayer.z) * Mathf.Rad2Deg;

        // 만약 현재 회전과 목표 회전이 별 차이가 없는 경우 회전시키지 않습니다.
        if(Mathf.Acos(Vector3.Dot(behaviorController.transform.forward, directionToPlayer)) * Mathf.Rad2Deg < _TURNANGLEMINYAW)
        {
            isSucceeded = true;
            yield break;
        }

        // 목표 회전값을 설정합니다.
        ownerCharacter.movement.targetYawAngle = yawAngle;

        DragonAnimController animController = ownerCharacter.animController as DragonAnimController;

        // 애니메이션 재생
        if (yawAngle < 0.0f) animController.PlayTurnLeftAnimation();
        else animController.PlayTurnRightAnimation();

        // 회전이 시작될 때 까지 대기합니다.
        yield return new WaitUntil(() => ownerCharacter.movement.isTurning);

        // 이동 태스크가 끝날 때까지 대기합니다.
        yield return new WaitWhile(() => ownerCharacter.movement.isTurning);

        // 행동 성공
        isSucceeded = true;

        
    }

}
