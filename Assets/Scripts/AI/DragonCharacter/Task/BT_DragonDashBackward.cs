using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BT_DragonDashBackward : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        DragonCharacter ownerCharacter = behaviorController.GetKeyAsObject<EnemyCharacter>(
            BehaviorController_Enemy.KEYNAME_OWNERENEMYCHARACTER) as DragonCharacter;

        // 애니메이션 재생
        DragonAnimController animController = ownerCharacter.animController as DragonAnimController;
        animController.PlayDashBackwardAnimation();

        // Dash 가 시작될 때까지 대기합니다.
        yield return new WaitUntil(() => ownerCharacter.movement.isDashBackward);
        
        // Dash 가 끝날 때까지 대기합니다.
        yield return new WaitWhile(() => ownerCharacter.movement.isDashBackward);


        isSucceeded = true;
    }
}
