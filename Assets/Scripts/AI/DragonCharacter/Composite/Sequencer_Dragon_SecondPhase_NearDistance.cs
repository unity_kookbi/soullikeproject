using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequencer_Dragon_SecondPhase_NearDistance : BehaviorSequencer
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 플레이어와 캐릭터와의 거리를 얻습니다.
        float distanceToPlayer = behaviorController.GetKeyAsValue<float>(
            BehaviorController_Dragon.KEYNAME_DISTANCETOPLAYER);

        // 가까운 거리가 아니라면 실행 실패
        if (distanceToPlayer > DragonCharacter.NEARDISTANCE)
            return false;

        // 플레이어를 바라보는 방향으로 회전합니다.
        AddBehavior(() => new BT_DragonTurn(BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER));

        // 뒤로 이동합니다.
        AddBehavior<BT_DragonDashBackward>();

        // 불 뿜기
        AddBehavior<BT_SpreadFire>();

        return base.OnRunnableInitialized(behaviorController);
    }
}
