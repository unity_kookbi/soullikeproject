using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Sequencer_Dragon_FirstPhase_FarDistance : BehaviorSequencer
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 플레이어를 바라보는 방향으로 회전합니다.
        AddBehavior(() => new BT_DragonTurn(BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER));

        // 캐릭터 위치를 얻습니다.
        AddBehavior(() => new BT_GetPlayerPosition(
            BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION,
            BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER,
            null, false));

        AddBehavior(() => new BT_WaitSeconds(0.5f));

        AddBehavior(() => new BT_MoveTo(BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION,true,
            (targetPos) =>
            {
                // 목표로 향하는 방향을 얻습니다.
                Vector3 direction = targetPos - behaviorController.transform.position;
                direction.y = 0.0f;
                direction.Normalize();

                // 최대 속도로 이동을 시작합니다.
                NavMeshAgent agent = (behaviorController as BehaviorController_Enemy).agent;
                agent.velocity = direction * agent.speed;
            }));

        return base.OnRunnableInitialized(behaviorController);
    }
}
