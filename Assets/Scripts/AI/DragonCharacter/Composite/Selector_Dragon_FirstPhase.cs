using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector_Dragon_FirstPhase : BehaviorSelector
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 거리가 가까운 경우
        AddBehavior<Sequencer_Dragon_FirstPhase_NearDistance>();

        // 거리가 먼 경우
        AddBehavior<Sequencer_Dragon_FirstPhase_FarDistance>();


        return base.OnRunnableInitialized(behaviorController);
    }
}
