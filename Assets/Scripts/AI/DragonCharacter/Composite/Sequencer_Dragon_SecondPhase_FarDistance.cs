using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequencer_Dragon_SecondPhase_FarDistance : BehaviorSequencer
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 플레이어를 바라보는 방향으로 회전합니다.
        AddBehavior(() => new BT_DragonTurn(BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER));

        // 불 뿜기
        AddBehavior<BT_SpreadFire>();

        // 뒤로 이동합니다.
        AddBehavior<BT_DragonDashBackward>();

        return base.OnRunnableInitialized(behaviorController);
    }

}
