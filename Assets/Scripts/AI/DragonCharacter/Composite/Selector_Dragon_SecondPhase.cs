using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector_Dragon_SecondPhase : BehaviorSelector
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 체력을 얻습니다.
        EnemyCharacter ownerEnemyCharacter = behaviorController.GetKeyAsObject<EnemyCharacter>(BehaviorController_Enemy.KEYNAME_OWNERENEMYCHARACTER);

        // Second Phase 구간으로 넘어가는 체력이 아닌 경우 실행 실패
        //if(ownerEnemyCharacter.currentHp > DragonCharacter.SECOND_PHASE_HP)
        //    return false;

        // 거리가 가까운 경우
        AddBehavior<Sequencer_Dragon_SecondPhase_NearDistance>();

        // 거리가 먼 경우
        AddBehavior<Sequencer_Dragon_SecondPhase_FarDistance>();

        return base.OnRunnableInitialized(behaviorController);
    }
}
