using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootSelector_Dragon : BehaviorSelector
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 체력이 얼마 남지 않은 경우
        AddBehavior<Selector_Dragon_SecondPhase>();

        // 체력이 많은 경우
        AddBehavior<Selector_Dragon_FirstPhase>();

        return base.OnRunnableInitialized(behaviorController);
    }
}
