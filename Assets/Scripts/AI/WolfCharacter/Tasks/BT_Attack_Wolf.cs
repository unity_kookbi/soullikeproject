using System.Collections;
using UnityEngine;

public class BT_Attack_Wolf : RunnableBehavior
{
    public override IEnumerator RunBehavior()
    {
        // 공격 요청을 처리합니다.
        behaviorController.SetKey(BehaviorController_Wolf.KEYNAME_ATTACKREQUESTED, false);

        // WolfCharacter의 공격 컴포넌트를 얻습니다.
        WolfAttack attackComponent = behaviorController.GetComponent<WolfAttack>();

        // 공격을 실행
        attackComponent.OnAttackStarted();

        // 공격이 끝날때까지 대기
        yield return new WaitWhile(() => attackComponent.isAttacking);

        // 행동 성공
        isSucceeded = true;
        yield return null;
    }
}