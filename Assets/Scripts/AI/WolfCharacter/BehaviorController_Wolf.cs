using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Wolf 캐릭터 행동 제어 컴포넌트입니다.
/// </summary>
public sealed class BehaviorController_Wolf : BehaviorController_Enemy
{
    /// <summary>
    /// 공격이 요청되었음을 나타내기 위한 키
    /// </summary>
    public const string KEYNAME_ATTACKREQUESTED = "AttackRequested";


    protected override void Awake()
    {
        base.Awake();

        AddKey(KEYNAME_ATTACKREQUESTED, false);     // 공격 요청됨 상태 키 추가

        SetKey(KEYNAME_MOVETARGETPOSITION, transform.position);
        SetKey(KEYNAME_MAXMOVEDISTANCE, 10.0f);
        SetKey(KEYNAME_MAXTRACKINGDISTANCE, 5.0f);

    }

    protected override void Start()
    {
        base.Start();

        StartBehavior();
    }

    public override void StartBehavior()
    {
        StartBehavior<RootSelector_Wolf>();
    }
}
