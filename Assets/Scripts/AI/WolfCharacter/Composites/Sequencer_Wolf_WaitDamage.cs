
using System.Collections;
/// <summary>
/// Wolf Character 의 대기 상태 시 실행될 Sequencer 입니다.
/// </summary>
public sealed class Sequencer_Wolf_WaitDamage : BehaviorSequencer
{
    public Sequencer_Wolf_WaitDamage()
    {
        // 1초 대기
        AddBehavior(() => new BT_WaitSeconds(1.0f));

        // 랜덤한 위치를 선택합니다.
        AddBehavior(() => new BT_GetRandomPositionInNavigableRadius(
            BehaviorController_Wolf.KEYNAME_MOVETARGETPOSITION,
            BehaviorController_Wolf.KEYNAME_SPAWNPOSITION,
            BehaviorController_Wolf.KEYNAME_MAXMOVEDISTANCE));

        // 선택한 위치로 이동
        AddBehavior(() => new BT_MoveTo(BehaviorController_Wolf.KEYNAME_MOVETARGETPOSITION));
    }

    public override IEnumerator RunBehavior()
    {
        // 공격적인 상태인지 확인합니다.
        bool isAggressiveState = behaviorController.GetKeyAsValue<bool>(
            BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE);

        // 공격적인 상태인 경우 행동 실패
        if (isAggressiveState) 
        {
            isSucceeded = false;
            yield break;
        }

        // 기본 로직 실행
        yield return base.RunBehavior();
    }
}