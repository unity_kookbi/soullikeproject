

public sealed class RootSelector_Wolf : BehaviorSelector
{
    public RootSelector_Wolf()
    {
        // 대기 상태
        AddBehavior<Sequencer_Wolf_WaitDamage>();

        // 공격 상태
        AddBehavior<Selector_Wolf_Aggressive>();
        
    }


}