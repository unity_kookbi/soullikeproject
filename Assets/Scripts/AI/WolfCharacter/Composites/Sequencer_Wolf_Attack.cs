

public class Sequencer_Wolf_Attack : BehaviorSequencer
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 회전 행동
        AddBehavior(() => new BT_LookAt(BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER));

        // 공격 행동
        AddBehavior<BT_Attack_Wolf>();

        return base.OnRunnableInitialized(behaviorController);
    }
}
