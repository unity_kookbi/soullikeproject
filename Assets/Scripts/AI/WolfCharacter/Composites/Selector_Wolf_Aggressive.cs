using UnityEngine;

public sealed class Selector_Wolf_Aggressive : BehaviorSelector
{
    public override bool OnRunnableInitialized(BehaviorController behaviorController)
    {
        // 추적 이동
        AddBehavior(() => new Sequencer_Wolf_TrackingPlayer(
            BehaviorController_Wolf.KEYNAME_ATTACKREQUESTED));

        // 공격 행동
        AddBehavior<Sequencer_Wolf_Attack>();

        // 기존 로직 실행
        return base.OnRunnableInitialized(behaviorController);
    }

}