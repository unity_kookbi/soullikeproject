

using System.Collections;
using UnityEngine;

/// <summary>
///  지정된 시간만큼 대기하는 행동입니다.
/// </summary>
public class BT_WaitSeconds : RunnableBehavior
{
    /// <summary>
    /// 지정한 시간만큼 대기하는 객체입니다.
    /// </summary>
    private WaitForSeconds _WaitSeconds;

    public BT_WaitSeconds(float waitSeconds)
    {
        // waitSeconds 만큼 대기하도록 합니다.
        _WaitSeconds = new WaitForSeconds(waitSeconds);
    }

    public override IEnumerator RunBehavior()
    {
        // 시간 대기
        yield return _WaitSeconds;

        // 행동 성공
        isSucceeded = true;
    }
}