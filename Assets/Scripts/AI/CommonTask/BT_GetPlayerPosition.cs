using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class BT_GetPlayerPosition : RunnableBehavior
{
    /// <summary>
    /// 목표 위치 키 이름
    /// </summary>
    private string _TargetPositionKey;

    /// <summary>
    /// 플레이어 캐릭터를나타내는 키 이름
    /// </summary>
    private string _PlayerCharacterKey;

    /// <summary>
    /// 최대 추적거리를 나타내는 키 이름
    /// </summary>
    private string _MaxTrackingDistanceKey;

    /// <summary>
    ///  최대 추적 거리 사용 여부
    /// </summary>
    private bool _UseMaxTrackingDistance;

    public BT_GetPlayerPosition(
        string targetPositionKey,
        string playerCharacterKey,
        string maxTrackingDistanceKey,
        bool useMaxTrackingDistance = true)
    {
        _TargetPositionKey = targetPositionKey;
        _PlayerCharacterKey = playerCharacterKey;
        _MaxTrackingDistanceKey = maxTrackingDistanceKey;
        _UseMaxTrackingDistance = useMaxTrackingDistance;
    }

    public override IEnumerator RunBehavior()
    {
        // 플레이어 캐릭터 객체를 얻습니다.
        PlayerCharacter playerCharacter =
            behaviorController.GetKeyAsObject<PlayerCharacter>(_PlayerCharacterKey);

        // 플레이어 캐릭터를 가져오지 못한 경우 행동 실패
        if(!playerCharacter)
        {
            isSucceeded = false;
            yield break;
        }

        // 목표 위치를 얻습니다.
        Vector3 targetPosition = playerCharacter.transform.position;

        // 현재 위치를 얻습니다.
        Vector3 currentPosition = behaviorController.transform.position;

        // 목표까지의 거리를 구합니다.
        float distanceToTarget = Vector3.Distance(targetPosition, currentPosition);

        if(_UseMaxTrackingDistance)
        {
            // 최대 추적 거리
            float maxTrackingDistance = behaviorController.GetKeyAsValue<float>(_MaxTrackingDistanceKey);

            // 목표까지의 거리가 최대 추적 거리보다 먼 경우
            if(distanceToTarget > maxTrackingDistance)
            {
                // 목표 위치를 재설정합니다.
                targetPosition = currentPosition + (targetPosition - currentPosition).normalized * maxTrackingDistance;
            }
        }

        // 목표 위치를 설정합니다.
        behaviorController.SetKey(_TargetPositionKey, targetPosition);

        isSucceeded = true;
    }
}
