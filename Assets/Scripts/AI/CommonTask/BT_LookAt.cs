using System.Collections;
using UnityEngine;

public class BT_LookAt : RunnableBehavior
{
    /// <summary>
    /// 목표 오브젝트를 나타내기 위한 키
    /// </summary>
    private string _TargetObjectKey;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="targetObjectKey">목표 오브젝트 키</param>
    public BT_LookAt(string targetObjectKey)
    {
        _TargetObjectKey = targetObjectKey;
    }

    public override IEnumerator RunBehavior()
    {
        // 목표 오브젝트를 얻습니다.
        GameObject targetObject = behaviorController.GetKeyAsObject<Component>(
            _TargetObjectKey)?.gameObject;

        // 목표 오브젝트를 찾지 목한 경우
        if(targetObject == null)
        {
            isSucceeded = false;
            yield break;
        }

        // 목표 오브젝트 위치
        Vector3 targetPosition = targetObject.transform.position;
        targetPosition.y = 0.0f;

        // 현재 위치
        Vector3 currentPosition = behaviorController.transform.position;
        currentPosition.y = 0.0f;

        // 목표로 향하는 방향
        Vector3 direction = (targetPosition - currentPosition).normalized;

        // 목표를 바라보도록 하는 Yaw 회전값
        float yawAngle = Mathf.Atan2(direction.x,direction.z) * Mathf.Rad2Deg;

        // 현재 Yaw 회전값
        float currentYawAngle = behaviorController.transform.eulerAngles.y;

        // 회전속도
        float angularSpeed = (behaviorController as BehaviorController_Enemy).agent.angularSpeed;

        // 부드럽게 회전시킨 다음 Yaw 회전값을 계산합니다.
        currentYawAngle = Mathf.MoveTowardsAngle(currentYawAngle, yawAngle, angularSpeed * 10.0f * Time.deltaTime);

        // 회전값을 적용합니다.
        behaviorController.transform.rotation = Quaternion.Euler(Vector3.up * currentYawAngle);

        // 목표값까지 회전되었을 경우 행동 성공
        isSucceeded = currentYawAngle == yawAngle;

    }
}