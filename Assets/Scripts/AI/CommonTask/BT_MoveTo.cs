using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class BT_MoveTo : RunnableBehavior
{
    /// <summary>
    /// 목표 위치 키입니다.
    /// </summary>
    private string _TargetPositionKey;

    /// <summary>
    /// 이동이 끝날 때 까지 대기합니다.
    /// </summary>
    private WaitUntil _WaitMoveFinish;

    /// <summary>
    /// 이동이 시작될 때 발생하는 이벤트
    /// </summary>
    private System.Action<Vector3/*targetPosition*/> _OnMoveStarted;

    private bool _StopAndStart;

    /// <summary>
    /// 목표 위치를 가지는 키 이름을 설정합니다.
    /// </summary>
    /// <param name="moveTargetPositionKey">목표 위치를 가지는 키 이름을 전달합니다.</param>
    public BT_MoveTo(string moveTargetPositionKey, bool stopAndStart = true, System.Action<Vector3> onMoveStarted = null)
    {
        // 목표 위치 키 설정
        _TargetPositionKey = moveTargetPositionKey;

        _StopAndStart = stopAndStart;

        // 이동이 끝날 때까지 대기하도록 합니다.
        _WaitMoveFinish = new WaitUntil(
            () => (behaviorController as BehaviorController_Enemy).agent.isStopped);

        // 이동 시작 이벤트
        _OnMoveStarted = onMoveStarted;
    }

    public override IEnumerator RunBehavior()
    {
        NavMeshAgent agent = (behaviorController as BehaviorController_Enemy).agent;

        // 이동 목표 위치를 얻습니다.
        Vector3 targetPosition = behaviorController.GetKeyAsValue<Vector3>(_TargetPositionKey);

        // 이동 시작 이벤트 발생
        _OnMoveStarted?.Invoke(targetPosition);

        // 이동 목표 위치를 설정합니다.
        agent.SetDestination(targetPosition);

        if(_StopAndStart)
        {
            agent.isStopped = false;

            // 목표 위치에 도달할때까지 대기합니다.
            yield return _WaitMoveFinish;
        }

        isSucceeded = true;
    }
}