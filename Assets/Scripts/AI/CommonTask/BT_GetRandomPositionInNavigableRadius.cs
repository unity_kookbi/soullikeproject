using System.Collections;
using UnityEngine;
using UnityEngine.AI;


/// <summary>
/// 지정한 반경 내에서 이동 가능한 랜덤한 위치를 뽑습니다.
/// </summary>
public class BT_GetRandomPositionInNavigableRadius : RunnableBehavior
{
    /// <summary>
    /// 랜덤한 위치를 뽑아 저장시킬 키 이름을 나타냅니다.
    /// </summary>
    private string _ResultKey;

    /// <summary>
    /// 기준 위치를 나타내는 키 이름
    /// </summary>
    private string _OriginPositionKey;

    /// <summary>
    /// 이동 가능한 반경을 나타내는 키 이름.
    /// </summary>
    private string _MaxMovableDistanceKey;

    /// <summary>
    /// 랜덤한 위치를 뽑기에 실패한 경우, 시도 가능한 횟수를 나타냅니다.
    /// </summary>
    private int _NumberOfAttempts;

    /// <summary>
    /// 랜덤한 위치를 뽑아 위치를 저장시킬 키 이름을 설정합니다.
    /// </summary>
    /// <param name="resultKey"></param>
    /// <param name="originPositionKey">랜덤한 위치를 뽑기 위하여 기준이 되는 위치를 전달합니다.</param>
    /// <param name="maxMovableDistanceKey">이동 가능 반경을 전달합니다.</param>
    /// <param name="numberOfAttempts">랜덤한 위치를 성공적으로 뽑기 위하여 시도 횟수를 전달합니다.</param>
    public BT_GetRandomPositionInNavigableRadius(
        string resultKey,
        string originPositionKey,
        string maxMovableDistanceKey,
        int numberOfAttempts = 50)
    {
        _ResultKey = resultKey;
        _OriginPositionKey = originPositionKey;
        _MaxMovableDistanceKey = maxMovableDistanceKey;
        _NumberOfAttempts = numberOfAttempts;
    }

    public override IEnumerator RunBehavior()
    {
        for (int i = 0; i < _NumberOfAttempts; ++i)
        {


            // 랜덤한 거리
            float distance = Random.Range(0.0f, behaviorController.GetKeyAsValue<float>(_MaxMovableDistanceKey));

            // 랜덤한 방향
            Vector3 direction = Random.insideUnitSphere;
            // insideUnitSphere : 반지름이 1인 구 내에서 랜덤한 위치를 뽑아 반환합니다.
            direction.y = 0.0f;
            direction.Normalize();

            // 확인시킬 위치
            Vector3 checkPosition = behaviorController.GetKeyAsValue<Vector3>(_OriginPositionKey) + (direction * distance);

            // 이동할 수 있는 위치를 뽑은 경우
            if (NavMesh.SamplePosition(
                checkPosition,                                                          // 확인할 위치를 전달합니다.
                out NavMeshHit hitResult,                                               // 확인 결과를 저장할 변수를 전달합니다.
                behaviorController.GetKeyAsValue<float>(_MaxMovableDistanceKey),        // 확인할 수 있는 최대 거리를 전달합니다.
                NavMesh.AllAreas))                                                      // 위치를 확인할 영역을 전달합니다.
            {
                // 목표 위치로 설정합니다.
                behaviorController.SetKey(
                    _ResultKey, hitResult.position);


                // 행동 성공
                isSucceeded = true;
                yield break;
            }
        }

        isSucceeded = false;
    }
}