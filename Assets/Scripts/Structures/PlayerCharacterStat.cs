
using UnityEngine;

[System.Serializable]
public struct PlayerCharacterStat
{
    [Header("# 기본 최대 체력")]
    public float m_MaxHp;

    [Header("# 기본 공격력")]
    public float m_Atk;

}