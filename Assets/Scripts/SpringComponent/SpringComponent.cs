using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringComponent : MonoBehaviour
{
    [Header("# 스프링 거리")]
    [Range(0.0f, 5.0f)]
    public float m_SpringDistanceMin = 1.0f;
    [Range(1.0f, 30.0f)]
    public float m_SpringDistanceMax = 3.0f;

    [Header("# 스프링 오프셋")]
    public Vector3 m_SpringOffset;

    [Header("# 충돌체 감지 관련")]
    /// <summary>
    /// 충돌체 감지에서 제외시킬 레이어
    /// </summary>
    public LayerMask m_ExclusionLayer;

    [Header("# 회전 관련")]
    public float m_TurnSpeedMultiplier = 0.2f;
    public float m_PitchAngleLimitMax = 80.0f;
    public float m_PitchAngleLimitMin = -80.0f;

    /// <summary>
    /// 스프링 이동시킬 트랜스폼을 나타냅니다.
    /// </summary>
    private Transform _SpringTargetTransform;

    /// <summary>
    /// 현재 적용된 Yaw 회전값을 나타냅니다.
    /// </summary>
    private float _CurrentYawAngle;

    /// <summary>
    /// 현재 적용된 Pitch 회전값을 나타냅니다.
    /// </summary>
    private float _CurrentPitchAngle;

    /// <summary>
    /// 사용자가 설정한 거리이며, 실제로 적용될 거리입니다.
    /// </summary>
    public float currentLength { get; set; }

    #region DEBUG
    private DrawGizmoLineInfo _DrawCheckCollision;
    #endregion

    private void Awake()
    {
        if(m_SpringDistanceMin > m_SpringDistanceMax)
        {
            m_SpringDistanceMax = m_SpringDistanceMin;
        }
        currentLength = (m_SpringDistanceMax + m_SpringDistanceMin) * 0.5f;
    }

    private void Update()
    {
        // 충돌체 확인
        float length = DoCollisionTest();

        // 길이 갱신
        UpdateSpringLength(length);

        // 회전 갱신
        UpdateRotation();
    }

    /// <summary>
    /// 스프링 길이를 생신합니다.
    /// </summary>
    /// <param name="length"></param>
    private void UpdateSpringLength(float length)
    {
        _SpringTargetTransform.localPosition = Vector3.back * length;
    }

    private void UpdateRotation()
    {
        transform.rotation = Quaternion.Euler(_CurrentPitchAngle, _CurrentYawAngle, 0.0f);
    }

    /// <summary>
    /// 충돌 검사를 진행합니다.
    /// </summary>
    /// <returns>충돌체가 감지된다면 충돌체까지의 거리를 반환하고,
    /// 그렇지 않다면 사용자가 설정한 거리(currentLength) 를 반환합니다.</returns>
    private float DoCollisionTest()
    {
        // 스프링 컴포넌트의 위치를 레이 시작점으로 설정합니다.
        Vector3 rayStart = transform.position;

        // 레이를 발사시킬 방향
        Vector3 rayDirection = transform.forward * -1;

        // 레이 구조체를 완성합니다.
        Ray ray = new Ray(rayStart, rayDirection);

        // 충돌 검사에 포함시킬 레이어
        int inclusiveLayer = ~m_ExclusionLayer;

        // 레이캐스트를 진행합니다.
        RaycastHit hitResult;
        bool isHit = PhysicsExt.Raycast(out _DrawCheckCollision, ray, out hitResult, currentLength, inclusiveLayer, QueryTriggerInteraction.Ignore);

        // 감지된 객체가 존재하는 경우 감지 거리를 반환
        // 아니라면 사용자가 설정한 거리를 반환합니다.
        return (isHit ? hitResult.distance : currentLength);
    }

    /// <summary>
    /// 스프링 이동시킬 트랜스폼을 설정합니다.
    /// </summary>
    /// <param name="newTarget"></param>
    public void SetTargetTransform(Transform newTarget)
    {
        _SpringTargetTransform = newTarget;

        _SpringTargetTransform.SetParent(transform);

        _SpringTargetTransform.localPosition = Vector3.zero;
        _SpringTargetTransform.rotation = Quaternion.identity;
    }

    public void Turn(float addYawAngle, float addPitchAngle)
    {
        _CurrentYawAngle += addYawAngle * m_TurnSpeedMultiplier;
        _CurrentPitchAngle -= addPitchAngle * m_TurnSpeedMultiplier;

        // _CurrentPitchAngle 값 범위 설정
        _CurrentPitchAngle = Mathf.Clamp(_CurrentPitchAngle, m_PitchAngleLimitMin, m_PitchAngleLimitMax);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        PhysicsExt.DrawGizmoLine(in _DrawCheckCollision);
    }
#endif
}
