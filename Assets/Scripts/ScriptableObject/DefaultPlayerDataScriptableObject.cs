using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 플레이어 캐릭터의 기본 능력치를 정의합니다.
[CreateAssetMenu(fileName ="DefaultPlayerData", menuName = "ScriptableObject/PlayerData")]
public sealed class DefaultPlayerDataScriptableObject : ScriptableObject
{
    /// <summary>
    /// 캐릭터 기본 능력치
    /// </summary>
    public PlayerCharacterStat m_DefaultCharacterStat;

}
