using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "ScriptableObject/EnemyData")]
public class EnemyDataScriptableObject : ScriptableObject
{
    public List<EnemyDataElem> m_EnemyDatas;

    public EnemyData GetEnemyData(string enemyCode)
        => m_EnemyDatas.Find(elem => elem.m_EnemyCode == enemyCode).m_EnemyData;
}

[System.Serializable]
public class EnemyDataElem
{
    public string m_EnemyCode;
    public EnemyData m_EnemyData;
}

[System.Serializable]
public struct EnemyData
{
    [Header("# 적 이름")]
    public string enemyName;

    [Header("# 최대 체력")]
    public float maxHp;

    [Header("# 공격력")]
    public float atk;

}