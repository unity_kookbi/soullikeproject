using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 공격 데이터를 담는 ScriptableObject
/// </summary>
[CreateAssetMenu(fileName = "AttackData", menuName = "ScriptableObject/AttackData")]
public class AttackDataScriptableObject : ScriptableObject
{
    [Header("# 공격 데이터")]
    public List<AttackDataElem> m_AttackDatas;

    /// <summary>
    /// 공격 이름을 이용하여 공격 데이터를 얻습니다.
    /// </summary>
    /// <param name="attackName">공격 이름</param>
    /// <returns></returns>
    public AttackDataElem GetAttackDataFromName(string attackName)
        => m_AttackDatas.Find((AttackDataElem elem) => elem.m_AttackName == attackName);

}

[System.Serializable]
public class AttackDataElem
{
    [Header("# 공격 이름")]
    public string m_AttackName;

    [Header("# 공격 데이터")]
    public AttackData m_AttackData;
}

[System.Serializable]
public struct AttackData
{
    // 최대 콤보 카운트
    public int maxComboCount;

    // 공격력에 적용될 수치
    public float atkMultiplier;
}
