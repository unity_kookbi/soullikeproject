
using UnityEngine;

public interface IDamageInterface
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="damageCauser">피해를 준 오브젝트</param>
    /// <param name="damage">피해량</param>
    void OnApplyDamage(GameObject damageCauser, float damage);


}
