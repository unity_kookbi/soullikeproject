using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneBounds : MonoBehaviour
{
    public string m_LoadSceneName;

    private Collider _Bound;

    private bool _IsLoadingOrUnLoading;

    private bool _IsLoaded;

    private void Awake()
    {
        _Bound = GetComponent<Collider>();
        _Bound.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player"))
        {
            // 씬을 로드합니다.
            StartCoroutine(LoadAsync());
        }

    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            // 씬을 언로드합니다.
            StartCoroutine(UnLoadAsync());
        }

    }

    private IEnumerator LoadAsync()
    {
        // 이미 로드되어있는 상태라면 루틴을 종료합니다.
        if(_IsLoaded) yield break;

        // 로딩 / 언로딩 상태가 끝날 때까지 대기합니다.
        yield return new WaitWhile(() => _IsLoadingOrUnLoading);

        _IsLoadingOrUnLoading = true;

        // 씬 비동기 로드를 진행합니다.
        AsyncOperation ao = SceneManager.LoadSceneAsync(m_LoadSceneName, LoadSceneMode.Additive);
        
        // 비동기 로드 작업이 끝날 때까지 대기합니다.
        yield return new WaitUntil(() => ao.isDone);

        // 로딩 / 언로딩 상태를 비활성화 시킵니다.
        _IsLoadingOrUnLoading = false;

        // 로드됨 상태로 설정합니다.
        _IsLoaded = true;

        Debug.Log($"{m_LoadSceneName} 로드됨");
    }

    private IEnumerator UnLoadAsync()
    {
        // 언로드 상태라면 루틴을 종료합니다.
        if (!_IsLoaded) yield break;

        // 로딩 / 언로딩 상태가 끝날 때까지 대기합니다.
        yield return new WaitWhile(() => _IsLoadingOrUnLoading);

        // 씬 언로드를 진행합니다.
        AsyncOperation ao = SceneManager.UnloadSceneAsync(m_LoadSceneName);
        
        // 언로드 작업이 끝날 때까지 대기합니다.
        yield return new WaitUntil(() => ao.isDone);
        

        // 언로드 상태를 설정합니다.
        _IsLoaded = false;

        // 작업 끝남
        _IsLoadingOrUnLoading = false;

        Debug.Log($"{m_LoadSceneName} 언로드됨");
    }
}
