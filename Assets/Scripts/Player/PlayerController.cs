using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : PlayerControllerBase
{
    /// <summary>
    /// 기본 플레이어 정보 ScriptableObject Asset
    /// TODO 추후 LocalDataManager 생성 후 위치변경
    /// </summary>
    private static DefaultPlayerDataScriptableObject _DefaultPlayerDataScriptableOnject;

    /// <summary>
    /// 플레이어 캐릭터에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public PlayerCharacter playerCharacter => controlledCharacter as PlayerCharacter;

    private PlayerInput _PlayerInput;

    /// <summary>
    /// 플레이어 캐릭터 능력치를 나타냅니다.
    /// </summary>
    private PlayerCharacterStat _PlayerCharacterStat;

    /// <summary>
    /// 플레이어 캐릭터 능력치에 대한 읽기 전용 프로퍼티
    /// </summary>
    public ref PlayerCharacterStat playerCharacterStat => ref _PlayerCharacterStat;

    #region 더블 클릭
    private float _RollingMovementLastInputTime;
    private Vector2Int _PrevRollingInputDirection;
    #endregion

    private void Awake()
    {
        if(_DefaultPlayerDataScriptableOnject == null)
        {
            _DefaultPlayerDataScriptableOnject =
                Resources.Load<DefaultPlayerDataScriptableObject>("ScriptableObject/DefaultPlayerData");
        }

        _PlayerInput = GetComponent<PlayerInput>();

        SetControlMode(Constants.ACTIONMAP_GAMEPLAY_MODE);
    }

    public override void StartControlCharacter(PlayerCharacterBase controlCharacter)
    {
        base.StartControlCharacter(controlCharacter);

        // TODO
        // LocalDataManager 생성 후 방식변경
        _PlayerCharacterStat = _DefaultPlayerDataScriptableOnject.m_DefaultCharacterStat;

        // 플레이어 캐릭터 능력치 갱신
        (controlCharacter as PlayerCharacter).UpdatePlayerCharacterStat(in _PlayerCharacterStat);
    }

    private void OnMove(InputValue value)
    {
        Vector2 axisValue = value.Get<Vector2>();
        playerCharacter?.OnMovementInput(axisValue);
    }

    private void OnTurn(InputValue value)
    {
        Vector2 turnAxis = value.Get<Vector2>();
        playerCharacter?.OnTurnInput(turnAxis);
    }

    private void OnJump()
    {
        playerCharacter?.OnJumpInput();
    }

    private void OnRunPressed()
    {
        playerCharacter?.OnRunPressed();
    }

    private void OnRunReleased() 
    {
        playerCharacter?.OnRunReleased();
    }

    private void OnRollForward()
    {
        // 입력 축 값
        Vector2Int inputDirection = new Vector2Int(0, 1);

        // 같은 방향으로 두번 눌린 경우
        if (IsDoubleInput(inputDirection))
        {
            playerCharacter?.OnRollingMovement(inputDirection);
        }
    }
    private void OnRollBackWard()
    {
        // 입력 축 값
        Vector2Int inputDirection = new Vector2Int(0, -1);

        // 같은 방향으로 두번 눌린 경우
        if (IsDoubleInput(inputDirection))
        {
            playerCharacter?.OnRollingMovement(inputDirection);
        }
    }
    private void OnRollLeft()
    {
        // 입력 축 값
        Vector2Int inputDirection = new Vector2Int(-1, 0);

        // 같은 방향으로 두번 눌린 경우
        if (IsDoubleInput(inputDirection))
        {
            playerCharacter?.OnRollingMovement(inputDirection);
        }
    }
    private void OnRollRight()
    {
        // 입력 축 값
        Vector2Int inputDirection = new Vector2Int(1,0);

        // 같은 방향으로 두번 눌린 경우
        if(IsDoubleInput(inputDirection))
        {
            playerCharacter?.OnRollingMovement(inputDirection);
        }
    }

    private void OnAttackInput()
    {
        playerCharacter?.OnAttackInput();
    }

    private void OnGuardPressed()
    {
        playerCharacter?.OnGuardStart();
    }

    private void OnGuardReleased() 
    {
        playerCharacter?.OnGuardFinished();
    }

    /// <summary>
    /// 중복 입력되었음을 확인합니다. (더블 클릭)
    /// 구르기에 사용되는 메서드입니다.
    /// </summary>
    /// <param name="inputAxis">입력된 축 값을 전달합니다.</param>
    /// <returns>중복 입력됨 여부를 반환합니다.</returns>
    private bool IsDoubleInput(Vector2Int inputAxis)
    {
        // 이전 입력과 동일한 입력이 들어온 경우
        if(_PrevRollingInputDirection == inputAxis &&
            // 다음 입력된 시간이 마지막으로 입력된 시간으로부터 텀 만큼 지나지 않은 경우
            Time.time <= _RollingMovementLastInputTime + Constants.DOUBLECLICK_MAXTERM)
        {
            _PrevRollingInputDirection = Vector2Int.zero;

            // 더블 클릭됨
            return true;
        }
        // 다른 입력이 들어온 경우
        else
        {
            // 입력 축 값을 기록합니다.
            _PrevRollingInputDirection = inputAxis;

            // 마지막으로 입력된 시간을 기록합니다.
            _RollingMovementLastInputTime = Time.time;

            return false;
        }

    }

    public void SetControlMode(string actionMap)
    {
        // 액션 맵을 설정합니다.
        _PlayerInput.SwitchCurrentActionMap(actionMap);

        switch (actionMap) 
        {
            case Constants.ACTIONMAP_GAMEPLAY_MODE:
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                break;

            case Constants.ACTIONMAP_UI_MODE:
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                break;
        }

    }

}
