using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : PlayerCharacterBase, IDamageInterface
{
    [Header("# 스프링 오브젝트 프리팹")]
    public SpringComponent m_SpringComponentPrfab;

    private PlayerCharacterMovement _MovementComponent;

    private PlayerCharacterAnimController _AnimController;

    private PlayerCharacterAttack _AttackComponent;

    /// <summary>
    /// 현재 체력
    /// </summary>
    public float currentHp { get; set; }

    /// <summary>
    /// 피해를 입는 상태임을 나타냅니다.
    /// </summary>
    public bool isHit { get; set; }

    public PlayerCharacterMovement movementComponent =>
        _MovementComponent ?? (_MovementComponent = GetComponent<PlayerCharacterMovement>());

    public PlayerCharacterAnimController animController =>
        _AnimController ?? (_AnimController = GetComponentInChildren<PlayerCharacterAnimController>());

    public PlayerCharacterAttack attackComponent =>
        _AttackComponent ?? (_AttackComponent = GetComponent<PlayerCharacterAttack>());

    /// <summary>
    /// 스프링 객체를 나타냅니다.
    /// </summary>
    public SpringComponent springObject { get; private set; }

    /// <summary>
    /// 플레이어 캐릭터 능력치를 나타냅니다.
    /// </summary>
    public PlayerCharacterStat playerCharacterStat { get; private set; }

    private void Start()
    {
        InitializeSpringObject();
        InitializePlayerStatePanel();

        animController.onRollingMovementFinished += movementComponent.OnRollingMovementFinished;
        animController.onDashForward += (float power) => movementComponent.AddImpulse(transform.forward, power);
        animController.onAttackSequenceFinished += attackComponent.OnAttackSequenceFinished;
        animController.onNextAttackInputCheckStarted += attackComponent.StartAttackInputChecking;
        animController.onNextAttackInputCheckFinished += attackComponent.StopAttackInputChecking;
        animController.setAttackAreaEnable += attackComponent.SetAttackAreaEnable;
        animController.onHitFinished += OnHitFinished;

        // AttackComponent Events
        attackComponent.onAttackStarted += movementComponent.LookAtCameraDirection;
        attackComponent.onAttackStarted += () => movementComponent.AllowMovementInput(false);
        attackComponent.onAttackStarted += () => movementComponent.AllowRotation(false);
        attackComponent.onAttackFinished += () => movementComponent.AllowMovementInput(true);
        attackComponent.onAttackFinished += () => movementComponent.AllowRotation(true);
    }

    private void FixedUpdate()
    {
        UpdateSpringOnjectPosition();
    }

    private void Update()
    {
        UpdateAnimParamValues();
    }

    /// <summary>
    /// 플레이어 상태 패널을 초기화합니다.
    /// </summary>
    private void InitializePlayerStatePanel()
    {
        GameSceneInstance gameSceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        // 플레이어 컨트롤러 객체
        PlayerController controller = playerController as PlayerController;

        // 플레이어 상태 패널 초기화
        PlayerStatePanel playerStatePanel = gameSceneInstance.m_GameUI.playerUIPanel.playerStatePanel;
        playerStatePanel.InitializePlayerStatePanel(controller.playerCharacterStat.m_MaxHp);
    }

    /// <summary>
    /// 스프링 오브젝트를 초기화합니다.
    /// </summary>
    private void InitializeSpringObject()
    {
        // 스프링 오브젝트 복사 생성
        springObject = Instantiate(m_SpringComponentPrfab);

        // 스프링 타깃으로 설정시킬 카메라 객체를 얻습니다.
        GameSceneInstance sceneInst = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        Camera useCamera = sceneInst.m_UseCamera;

        // 스프링 타깃 설정
        springObject.SetTargetTransform(useCamera.transform);
    }

    /// <summary>
    /// 스프링 오브젝트 위치를 갱신합니다
    /// </summary>
    private void UpdateSpringOnjectPosition()
    {
        springObject.transform.position = transform.position + springObject.m_SpringOffset;
    }

    /// <summary>
    /// 애니메이터 파라미터 값을 갱신합니다.
    /// </summary>
    private void UpdateAnimParamValues()
    {
        // 현재 속력
        float velocityLength = movementComponent.velocity.magnitude;

        // 입력 축을 얻습니다.
        Vector3 animationAxis = new Vector3(
            movementComponent.inputVector.x, 0.0f, movementComponent.inputVector.y);

        // 속도 갱신
        animController.currentVelocity = animationAxis * velocityLength;

        // 땅에 닿음 상태 갱신
        animController.isGrounded = movementComponent.isGrounded;

        // 현재 공격 콤보 설정
        animController.SetCurrentCombo(attackComponent.currentAttackComboCount);

        // 공격 끝남 설정
        animController.SetAttackFinished(!attackComponent.isAttack);

        // 방어 상태 갱신
        animController.SetBlockState(attackComponent.isBlocked);
    }

    /// <summary>
    /// 플레이어 캐릭터 능력치를 갱신합니다.
    /// </summary>
    /// <param name="playerCharacterStat"></param>
    public void UpdatePlayerCharacterStat(in PlayerCharacterStat playerCharacterStat)
    {
        this.playerCharacterStat = playerCharacterStat;

        // 현재 체력 설정
        currentHp = playerCharacterStat.m_MaxHp;

        // 공격력 갱신
        attackComponent.atk = this.playerCharacterStat.m_Atk;
    }

    /// <summary>
    /// 이동 입력 시 호출되는 메서드입니다.
    /// </summary>
    /// <param name="inputAxis">입력 축 값이 전달됩니다.</param>
    public void OnMovementInput(Vector2 inputAxis)
    {
        movementComponent.OnMovementInput(inputAxis);
    }

    /// <summary>
    /// 마우스 회전 입력 시 호출되는 메서드입니다.
    /// </summary>
    /// <param name="turnAxis">마우스 이동 축 값이 전달됩니다.</param>
    public void OnTurnInput(Vector2 turnAxis)
    {
        springObject.Turn(turnAxis.x, turnAxis.y);
    }

    /// <summary>
    /// 점프 입력시 호출됩니다.
    /// </summary>
    public void OnJumpInput() => movementComponent.OnJumpInput();

    /// <summary>
    /// 달리기 입력
    /// </summary>
    public void OnRunPressed() => movementComponent.OnRunStart();

    public void OnRunReleased() => movementComponent.OnRunFinish();

    /// <summary>
    /// 구르기 입력
    /// </summary>
    /// <param name="rollingDirection">구르기 방향</param>
    public void OnRollingMovement(Vector2Int rollingDirection)
    {
        // 피해를 입는 상태라면 함수 호출 종료
        if (isHit) return;

        // 공격 상태라면 함수 호출 종료
        if (attackComponent.isAttack) return;

        // 이동 입력이 허용된 경우에만 구르기 실행
        if (!movementComponent.isMovementInputAllowed) return;

        // 땅에 당아있을 경우에만 실행되도록 합니다.
        if (!movementComponent.isGrounded) return;

        // 구르기 애니메이션 재생
        animController.PlayRollingAnimation(rollingDirection);


        // 뷰 앞 / 오른쪽 방향을 구합니다.
        Vector3 forward = springObject.transform.forward;
        Vector3 right = springObject.transform.right;

        // Y 축 값을 제외하고 계산합니다.
        forward.y = right.y = 0.0f;

        // 벡터 정규화
        forward.Normalize();
        right.Normalize();

        forward *= rollingDirection.y;
        right *= rollingDirection.x;

        // 캐릭터를 뷰 방향으로 회전시킵니다.
        float viewYawRotationYaw = movementComponent.GetViewRotationYaw();
        movementComponent.SetCharacterYawRotation(viewYawRotationYaw);

        // 캐릭터 기준 구르기 이동
        Vector3 direction = (forward + right).normalized;

        movementComponent.StartRollingMovement(direction);
    }

    /// <summary>
    /// 공격 입력
    /// </summary>
    public void OnAttackInput()
    {
        // 피해를 입는 상태라면 함수 호출 종료
        if (isHit) return;

        // 점프 중이라면 함수 호출 종료
        if (movementComponent.isJumping) return;

        // 방어중 상태라면 함수 호출 종료
        if (attackComponent.isBlocked) return;

        attackComponent.RequestAttack(Constants.PLAYERATTACKNAME_DEFAULT);
    }

    /// <summary>
    /// 가드 입력
    /// </summary>
    public void OnGuardStart()
    {
        // 피해를 입는 상태라면 함수 호출 종료
        if (isHit) return;

        // 점프 중이라면 함수 호출 종료
        if (movementComponent.isJumping) return;

        attackComponent.StartGuard();
    }

    public void OnGuardFinished() => attackComponent.FinishGuard();

    public void OnApplyDamage(GameObject damageCauser, float damage)
    {
        if (movementComponent.isRolling) return;

        // 방어 처리되었음을 확인합니다.
        bool isBlocked = IsBlocked(damageCauser, out bool perfectGuard);

        // 방어 상태인 경우 파티클 생성
        if (isBlocked)
        {
            float halfHeight = movementComponent.characterController.height * 0.5f;
            Vector3 position = transform.position;
            float floorHeight = position.y - halfHeight;

            attackComponent.PlayBlockParticle(floorHeight);
        }

        // 방어되지 못한 경우
        if (!isBlocked || !perfectGuard)
        {
            if(!isBlocked)
            {
                // 피해 입음 설정
                isHit = true;

                attackComponent.ClearAttackState();
                attackComponent.StopAttackInputChecking();

                movementComponent.AllowMovementInput(false);

                // Hit 애니메이션
                animController.SetHitState();
            }

            // Get Game Scene Instance
            GameSceneInstance gameSceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

            // 플레이어 상태 패널 초기화
            PlayerStatePanel playerStatePanel = gameSceneInstance.m_GameUI.playerUIPanel.playerStatePanel;

            // HP 감소
            if (isBlocked && !perfectGuard)
            {
                damage *= 0.5f;
            }

            currentHp -= damage;

            if (currentHp <= 0.0f)
            {
                currentHp = 0.0f;
                OnDead();
            }

            // UI 갱신
            playerStatePanel.UpdateHpValue(currentHp);
        }

        // 밀릴 방향
        Vector3 pushDirection = (transform.position - damageCauser.transform.position);
        pushDirection.y = 0.0f;
        pushDirection.Normalize();

        // 플레이어 넉백 처리
        movementComponent.AddImpulse(pushDirection, 5.0f, true);

    }

    /// <summary>
    /// 피해 입는 상태 끝
    /// </summary>
    private void OnHitFinished()
    {
        if (isHit)
        {
            isHit = false;

            // 공격 중이라면 공격 끝
            if (attackComponent.isAttack ||
                !attackComponent.isAttackInputChecking)
            {
                attackComponent.ClearAttackState();
            }

            if (!movementComponent.isMovementInputAllowed)
            {
                movementComponent.AllowMovementInput(true);
            }
        }
    }

    /// <summary>
    /// 공격이 방어 처리 되었음을 확인합니다.
    /// </summary>
    /// <param name="otherEnemy">상대 적 캐릭터를 전달합니다.</param>
    /// <returns></returns>
    private bool IsBlocked(GameObject otherEnemy, out bool perfectGuard)
    {
        perfectGuard = false;

        // 방어중임을 확인합니다.
        if (!attackComponent.isBlocked) return false;

        // 방어 시작 시간
        float guardStartTime = attackComponent.guardStartTime;

        Vector3 forward = transform.forward;

        Vector3 toEnemy = otherEnemy.transform.position - transform.position;

        forward.y = toEnemy.y = 0.0f;

        toEnemy.Normalize();

        float angle = Mathf.Acos(Vector3.Dot(forward, toEnemy)) * Mathf.Rad2Deg;

        // 방어 처리 가능한 각도인 경우
        if (angle <= Constants.PLAYER_BLOCKABLE_ANGLE * 0.5f)
        {
            // 완벽하게 공격을 막아낸 경우
            perfectGuard = (Time.time < guardStartTime + Constants.PROTECT_GUARD_TERMTIME);

            return true;
        }

        return false;
    }

    private void OnDead()
    {
        // 게임 오버 UI 띄우기

        // 렉돌 처리

    }
}
