﻿using UnityEngine;

/// <summary>
/// 플레이어 캐릭터 이동을 구현하기 위한 클래스입니다.
/// </summary>
public sealed class PlayerCharacterMovement : MonoBehaviour
{
    [Header("# 이동 관련")]
    public float m_AccelerationForce = 0.7f;
    public float m_BrakingForce = 0.8f;
    public float m_RunSpeed = 6.0f;
    public float m_WalkSpeed = 3.0f;
    public float m_RollingSpeed = 30.0f;


    [Header("# 회전 관련")]
    public float m_YawRotationSpeed = 360.0f;

    [Header("# 점프 관련")]
    /// <summary>
    /// 지형 충돌에서 제외시킬 레이어
    /// </summary>
    public LayerMask m_GroundCheckExclusionLayer;

    /// <summary>
    /// 중력에 곱해질 승수
    /// </summary>
    public float m_GravityMultiplier = 3.3f;

    /// <summary>
    /// 점프 힘
    /// </summary>
    public float m_JumpPower = 20.0f;

    /// <summary>
    /// 이동 입력 축 값입니다.
    /// x = R / L
    /// y = F / B
    /// </summary>
    private Vector2 _InputVector;

    /// <summary>
    /// 충격 속도입니다.
    /// </summary>
    private Vector3 _ImpulseVelocity;

    /// <summary>
    /// 현재 적용된 속도
    /// </summary>
    private Vector3 _CurrentVelocity;

    /// <summary>
    /// 목표 속도
    /// </summary>
    private Vector3 _TargetVelocity;

    /// <summary>
    /// 목표 Yaw 회전값
    /// </summary>
    private float _TargetYawAngle;

    /// <summary>
    /// 점프 입력이 들어왔음을 나타냅니다.
    /// </summary>
    private bool _IsJumpInput;

    /// <summary>
    /// 달리기 상태를 나타냅니다.
    /// </summary>
    private bool _IsRun;

    /// <summary>
    /// 이동 입력을 허용합니다.
    /// </summary>
    private bool _AllowMovementInput = true;

    /// <summary>
    /// 회전을 허용합니다.
    /// </summary>
    private bool _AllowRotation = true;

    /// <summary>
    /// 캐릭터 이동방향을 결정할 뷰 트랜스폼입니다.
    /// </summary>
    private Transform _ViewTransform;

    /// <summary>
    /// CharacterController Component 입니다.
    /// </summary>
    public CharacterController characterController { get; private set; }

    /// <summary>
    /// 현재 속도에 대한 읽기 전용 프로퍼티
    /// </summary>
    public Vector3 velocity => characterController.velocity;

    /// <summary>
    /// 축 입력값에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public Vector2 inputVector => _InputVector;

    /// <summary>
    /// 이동 허용 여부에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public bool isMovementInputAllowed => _AllowMovementInput;

    /// <summary>
    /// 땅에 닿아있음을 나타냅니다.
    /// </summary>
    public bool isGrounded { get; private set; }

    /// <summary>
    /// 점프중임을 나타냅니다.
    /// </summary>
    public bool isJumping { get; private set; }

    /// <summary>
    /// 구르기중임을 나타냅니다.
    /// </summary>
    public bool isRolling { get; private set; }

    #region DEBUG
    private DrawGizmoSphereInfo _DebugGroundCheck;
    #endregion

    private void Awake()
    {
        // 카메라 트랜스폼을 얻습니다.
        _ViewTransform = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>().m_UseCamera.transform;

        characterController = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        // 가속력 / 제동력 계산
        CalculateAccelerationOrBrakingForce();

        // 바닥 체크
        bool detectFloor = IsGrounded(out RaycastHit hitResult);

        if(!isGrounded && detectFloor)
        {
            _CurrentVelocity.y = 0.0f;
        }
        isGrounded = detectFloor;

        if(isJumping && isGrounded)
        {
            isJumping = false;
        }

        // 바라보는 방향으로 회전
        DesiredRotation();

        // 이동
        _CurrentVelocity += _ImpulseVelocity;
        _ImpulseVelocity = Vector3.Lerp(_ImpulseVelocity, Vector3.zero, m_BrakingForce * 2);

        // 경사면 계산
        CalculateSlopeAngle(in hitResult);

        // 중력을 계산합니다.
        CalculateGravity();

        //LimitVelocity();

        characterController.Move(_CurrentVelocity * Time.fixedDeltaTime);
    }

    private void Update()
    {
        // 입력을 목표 속도로 변환합니다.
        InputToTargetVelocity();
    }

    /// <summary>
    /// 입력 축 값을 목표 속도로 변환합니다.
    /// </summary>
    private void InputToTargetVelocity()
    {
        // 입력 벡터를 뷰 벡터로 변환합니다.
        Vector3 inputToViewDirection = ConvertInputDirectionToViewDirection(_InputVector, _ViewTransform);

        // 목표 속도를 설정합니다.
        // 달리기 중이라면 달리기 속력을, 아니라면 걷기 속력으로 설정합니다.
        _TargetVelocity = _AllowMovementInput ?
            inputToViewDirection * (_IsRun ? m_RunSpeed : m_WalkSpeed) :
            Vector3.zero;

    }

    /// <summary>
    /// 입력 방향을 뷰 방향으로 변환합니다.
    /// </summary>
    /// <param name="inputDirection">변환시킬 입력 축 값을 전달합니다.</param>
    /// <param name="viewTransform">변환의 기준이 될 뷰 트랜스폼을 전달합니다.</param>
    /// <returns></returns>
    private Vector3 ConvertInputDirectionToViewDirection(Vector2 inputDirection , Transform viewTransform)
    {
        Vector3 viewForward = viewTransform.forward;
        viewForward.y = 0.0f;
        viewForward.Normalize();
        viewForward *= inputDirection.y;

        Vector3 viewRight = viewTransform.right * inputDirection.x;

        return (viewForward + viewRight).normalized;
    }

    /// <summary>
    /// 가속력 또는 제동력을 연산합니다.
    /// </summary>
    private void CalculateAccelerationOrBrakingForce()
    {
        // 제동중임을 나타냅니다.
        bool isBraking = _CurrentVelocity.sqrMagnitude > _TargetVelocity.sqrMagnitude;

        Vector3 controllerVelocity = characterController.velocity;
        float yVelocity = controllerVelocity.y;
        controllerVelocity.y = 0.0f;

        Vector3 targetVelocity = _TargetVelocity;
        targetVelocity.y = 0.0f;

        // 현재 속도를 목표 속도로 변경합니다.
        _CurrentVelocity = Vector3.MoveTowards(
            controllerVelocity,
            targetVelocity,
            isBraking ? m_BrakingForce : m_AccelerationForce);

        _CurrentVelocity.y = yVelocity;

    }

    /// <summary>
    /// 바닥에 닿아있음을 확인합니다.
    /// </summary>
    /// <returns></returns>
    private bool IsGrounded(out RaycastHit hitResult)
    {
        // 바닥 검사를 위해 실행되는 Spherecast 길이
        float checkLength = characterController.height;

        // 바닥 검사를 위해 실행되는 Spherecast 구체 반지름
        float radius = characterController.radius;

        // 바닥 검사 레이어
        int groundCheckInclusiveLayer = ~m_GroundCheckExclusionLayer;

        // 아래를 향해 Spherecast 를 진행합니다
        bool isHit = PhysicsExt.SphereCast(
            out _DebugGroundCheck,
            new Ray(transform.position, Vector3.down),
            radius,
            out hitResult,
            checkLength,
            groundCheckInclusiveLayer,
            QueryTriggerInteraction.Collide);

        // 바닥을 감지한 경우
        if(isHit)
        {
            // 캐릭터 캡슐의 높이 절반을 계산합니다.
            float halfHeight = characterController.height * 0.5f;

            // 감지된 바닥의 Y 위치를 얻습니다.
            float detectedFloorYPos = hitResult.point.y;

            // 캐릭터 발 위치
            float characterBottomYPos = transform.position.y - halfHeight - characterController.skinWidth;

            // 캐릭터 발 위치가 바닥 위치보다 낮을 경우 바닥에 닿아있음
            return (detectedFloorYPos >= characterBottomYPos - 0.001f);
        }

        return false;
    }

    /// <summary>
    /// 중력을 계산합니다.
    /// </summary>
    private void CalculateGravity()
    {
        // 중력을 계산합니다.
        float engineGravity = Physics.gravity.y * m_GravityMultiplier * Time.fixedDeltaTime;

        // 땅에 닿아있는 경우
        if(isGrounded)
        {
            // 점프 입력이 들어온 상태라면
            if(_IsJumpInput)
            {
                _IsJumpInput = false;

                isJumping = true;

                _CurrentVelocity.y = m_JumpPower;
            }
        }
        // 땅에 닿아있지 않은 경우
        else
        {
            // 중력을 적용합니다.
            _CurrentVelocity.y += engineGravity;
            
        }
    }

    /// <summary>
    /// 바라보고 있는 방향으로 회전합니다.
    /// </summary>
    private void DesiredRotation()
    {
        // 회전을 허용하지 않는 상태인 경우 함수 호출 종료;
        if (!_AllowRotation) return;

        // 이동 입력이 없다면 회전시키지 않습니다.
        if (_InputVector.sqrMagnitude < Mathf.Epsilon) return;

        // 뷰 방향의 각도를 구합니다.
        float viewForwardYawAngle = GetViewRotationYaw();

        // 목표 회전을 설정합니다.
        _TargetYawAngle = viewForwardYawAngle;

        // 자연스럽게 회전시킵니다.
        float newYawAngle = Mathf.MoveTowardsAngle(
            transform.eulerAngles.y,
            _TargetYawAngle,
            m_YawRotationSpeed * Time.fixedDeltaTime);

        // 회전시킵니다.
        SetCharacterYawRotation(newYawAngle);
    }

    /// <summary>
    /// 경사각 계산
    /// </summary>
    private void CalculateSlopeAngle(in RaycastHit hitResult)
    {
        if(isGrounded && !isJumping)
        {
            // 경사면 확인
            bool isSlope = IsSlope(hitResult.normal);

            // 만약 경사면인 경우
            if(isSlope) 
            {
                // 이동 방향에 현재 경사각을 적용시킵니다.
                Vector3 slopeDirection = Vector3.ProjectOnPlane(_CurrentVelocity.normalized, hitResult.normal).normalized;

                // 현재 속력을 얻습니다.
                float velocityLength = _CurrentVelocity.magnitude;

                // 속도를 재정의합니다.
                _CurrentVelocity = slopeDirection * velocityLength;
            }
        }
    }

    /// <summary>
    /// 경사면임을 확인합니다.
    /// </summary>
    /// <param name="hitNormal">바닥의 노멀을 전달합니다.</param>
    /// <returns></returns>
    private bool IsSlope(Vector3 hitNormal)
    {
        // 캐릭터가 서있는 바닥의 경사각
        float floorAngle = Vector3.Angle(Vector3.up, hitNormal);

        // 오를 수 있는 최대 경사각
        float maxSlopeAngle = characterController.slopeLimit;

        // 오를 수 있는 최대 경사각보다 작은 각도인 경우, 경사면으로 인식하도록 합니다.
        return floorAngle < maxSlopeAngle;
    }

    /// <summary>
    /// 속도의 한계를 설정합니다.
    /// </summary>
    private void LimitVelocity()
    {
        // 충격 속력
        Vector3 xzImpulseVelocity = _ImpulseVelocity;
        xzImpulseVelocity.y = 0.0f;
        float impulseVelocityLength = xzImpulseVelocity.magnitude;

        // 이동 목표 속력
        Vector3 xzTargetVelocity = _TargetVelocity;
        xzTargetVelocity.y = 0.0f;
        float targetVelocityLength = xzTargetVelocity.magnitude;

        // 더 큰 속력을 최대 속력으로 사용합니다.
        float maxVelocityLength = 
            impulseVelocityLength > targetVelocityLength ? 
            impulseVelocityLength : targetVelocityLength;

        // 방향
        Vector3 direction = impulseVelocityLength > targetVelocityLength ?
            xzImpulseVelocity : xzTargetVelocity;
        direction.Normalize();
        
        Vector3 currentVelocity = velocity;
        currentVelocity.y = 0.0f;
        float currentVelocityLength = currentVelocity.magnitude;

        if(currentVelocityLength > maxVelocityLength) 
        {
            float currentYVelocity = _CurrentVelocity.y;
            _CurrentVelocity = direction * maxVelocityLength;
            _CurrentVelocity.y = currentYVelocity;
        }
    }

    /// <summary>
    /// 뷰 방향의 Yaw 회전값을 얻습니다.
    /// </summary>
    /// <returns></returns>
    public float GetViewRotationYaw()
    {
        // 뷰 방향의 각도를 구합니다.
        Vector3 viewForwardDirection = _ViewTransform.forward;
        viewForwardDirection.y = 0.0f;
        viewForwardDirection.Normalize();

        // Yaw 회전값 반환
        return Mathf.Atan2(viewForwardDirection.x, viewForwardDirection.z) * Mathf.Rad2Deg;
    }

    /// <summary>
    /// 캐릭터의 Yaw 회전을 설정합니다.
    /// </summary>
    /// <param name="yawAngle"></param>
    public void SetCharacterYawRotation(float yawAngle)
    {
        transform.rotation = Quaternion.Euler(0.0f, yawAngle, 0.0f);
    }

    /// <summary>
    /// 이동 입력을 처리하기 위한 메서드입니다.
    /// </summary>
    /// <param name="axisValue"></param>
    public void OnMovementInput(Vector2 axisValue)
    {
        _InputVector = axisValue;
    }

    public void OnJumpInput()
    {
        // 이동 입력이 가능하지 않은 상태라면 함수 호출 종료
        if(!_AllowMovementInput) return;

        // 점프 중이 아니며, 땅에 닿아있는 경우
        if(!_IsJumpInput && isGrounded)
        {
            _IsJumpInput = true;
        }
    }

    public void OnRunStart()
    {
        _IsRun = true;
    }

    public void OnRunFinish()
    {
        _IsRun = false;
    }

    /// <summary>
    /// 구르기 동작을 시작합니다.
    /// </summary>
    /// <param name="direction">방향을 전달합니다.</param>
    public void StartRollingMovement(Vector3 direction)
    {
        // 이동 입력을 블록시킵니다.
        _AllowMovementInput = false;

        // 회전을 블록시킵니다.
        _AllowRotation = false;

        // 구르기 상태로 설정합니다.
        isRolling = true;

        // 충격을 가합니다.
        AddImpulse(direction, m_RollingSpeed, true);
    }

    /// <summary>
    /// 구르기 동작이 끝났을 경우 호출되는 메서드입니다.
    /// </summary>
    public void OnRollingMovementFinished()
    {
        // 이동 입력을 허용합니다.
        _AllowMovementInput = true;

        // 회전을 허용합니다.
        _AllowRotation = true;

        // 구르기 종료 상태로 설정합니다.
        isRolling = false;
    }

    /// <summary>
    /// 특정 방향으로 충격을 가합니다.
    /// </summary>
    /// <param name="direction">충격을 가할 방향</param>
    /// <param name="power">힘</param>
    /// <param name="initializePrevVelocity">이전 속도를 초기화 시킬 것인지에 대한 여부</param>
    public void AddImpulse(Vector3 direction, float power, bool initializePrevVelocity = false)
    {
        Vector3 addVelocity = direction * power;
        _ImpulseVelocity = (initializePrevVelocity) ? addVelocity :
            _ImpulseVelocity + addVelocity;
    }

    /// <summary>
    /// 카메라 방향을 바라보도록 회전시킵니다.
    /// </summary>
    public void LookAtCameraDirection()
    {
        // 뷰의 Yaw 회전값을 얻습니다.
        _TargetYawAngle = _ViewTransform.eulerAngles.y;
       
        // 회전시킵니다.
        SetCharacterYawRotation(_TargetYawAngle);
    }

    public void AllowMovementInput(bool allow) => _AllowMovementInput = allow;

    public void AllowRotation(bool allow) => _AllowRotation = allow;
}

