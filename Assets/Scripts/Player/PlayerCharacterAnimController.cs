using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어 캐릭터 애니메이션을 제어하기 위한 컴포넌트입니다.
/// </summary>
public sealed class PlayerCharacterAnimController : AnimController
{
    private const string PARAMNAME_SPEEDX = "_SpeedX";
    private const string PARAMNAME_SPEEDY = "_SpeedY";
    
    private const string PARAMNAME_ISGROUNDED = "_IsGrounded";
    
    private const string PARAMNAME_ROLLX = "_RollX";
    private const string PARAMNAME_ROLLY = "_RollY";
    private const string PARAMNAME_ISROLLING = "_IsRolling";

    private const string PARAMNAME_CURRENTCOMBO = "_CurrentCombo";
    private const string PARAMNAME_ATTACKFINISHED = "_AttackFinished";
    private const string PARAMNAME_ISBLOCKED = "_IsBlocked";

    private const string PARAMNAME_HIT = "_Hit";

    private Vector3 _MoveSpeed;

    public bool isGrounded { get; set; }

    /// <summary>
    /// 현재 속도에 대한 프로퍼티입니다.
    /// </summary>
    public Vector3 currentVelocity {  get; set; }

    /// <summary>
    /// 구르기 이동 끝 이벤트
    /// </summary>
    public event System.Action onRollingMovementFinished;

    /// <summary>
    /// 하나의 공격이 끝났을 경우 발생하는 이벤트
    /// </summary>
    public event System.Action onAttackSequenceFinished;

    /// <summary>
    /// 다음 공격 입력 체크 시작 이벤트
    /// </summary>
    public event System.Action onNextAttackInputCheckStarted;

    /// <summary>
    /// 다음 공격 입력 체크 종료 이벤트
    /// </summary>
    public event System.Action onNextAttackInputCheckFinished;

    /// <summary>
    /// 공격 영역 활성화 / 비활성화
    /// </summary>
    public event System.Action<bool> setAttackAreaEnable;

    /// <summary>
    /// 앞 대시 이벤트
    /// </summary>
    public event System.Action<float /* power */> onDashForward;

    /// <summary>
    /// Hit 애니메이션 끝 이벤트
    /// </summary>
    public event System.Action onHitFinished;

    private void Update()
    {
        _MoveSpeed = Vector3.MoveTowards(_MoveSpeed, currentVelocity, 20.0f * Time.deltaTime);

        UpdateSpeedXZ(_MoveSpeed);
        UpdateGroundedState();
    }

    private void UpdateSpeedXZ(Vector3 velocityXZ)
    {
        SetFloat(PARAMNAME_SPEEDX, velocityXZ.x);
        SetFloat(PARAMNAME_SPEEDY, velocityXZ.z);
    }

    private void UpdateGroundedState()
    {
        SetBool(PARAMNAME_ISGROUNDED, isGrounded);
    }

    public void PlayRollingAnimation(Vector2Int rollDirection)
    {
        // 방향
        SetFloat(PARAMNAME_ROLLX, rollDirection.x);
        SetFloat(PARAMNAME_ROLLY, rollDirection.y);


        // 구르기 애니메이션 재생
        SetTrigger(PARAMNAME_ISROLLING);
    }

    public void SetCurrentCombo(int currentCombo) 
        => SetInt(PARAMNAME_CURRENTCOMBO, currentCombo);


    public void SetAttackFinished(bool isAttackFinished)
         => SetBool(PARAMNAME_ATTACKFINISHED, isAttackFinished);

    public void SetBlockState(bool block) 
        => SetBool(PARAMNAME_ISBLOCKED, block);

    public void SetHitState() => SetTrigger(PARAMNAME_HIT);


    /// <summary>
    /// 구르기 애니메이션이 끝났을 경우 발생하는 애니메이션 이벤트
    /// </summary>
    private void AnimEvent_OnRollingFinished() => onRollingMovementFinished?.Invoke();

    /// <summary>
    /// 공격이 끝났을 경우 발생하는 애니메이션 이벤트
    /// </summary>
    private void AnimEvent_OnDefaultAttackFinished() => onAttackSequenceFinished?.Invoke();

    /// <summary>
    /// 다음 공격 입력 확인을 시작합니다.
    /// </summary>
    private void AnimEvent_OnNextAttackInputCheckStarted() => onNextAttackInputCheckStarted?.Invoke();

    /// <summary>
    /// 다음 공격 입력 확인을 종료합니다
    /// </summary>
    private void AnimEvent_OnNextAttackInputCheckFinished() => onNextAttackInputCheckFinished?.Invoke();

    private void AnimEvent_OnDashForward(float power) => onDashForward?.Invoke(power);

    private void AnimEvent_EnableAttackArea() => setAttackAreaEnable?.Invoke(true);
    private void AnimEvent_DisableAttackArea() => setAttackAreaEnable?.Invoke(false);

    private void AnimEvent_OnHitFinished() => onHitFinished?.Invoke();

}
