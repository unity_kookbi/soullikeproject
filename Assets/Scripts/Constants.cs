﻿
using UnityEngine;

public static class Constants
{
    #region ActionMap Name
    public const string ACTIONMAP_GAMEPLAY_MODE = "GameInput";
    public const string ACTIONMAP_UI_MODE = "UIInput";
    #endregion

    #region 더블 클릭 관련
    // 더블클릭으로 인식시킬 최대 시간 텀
    public const float DOUBLECLICK_MAXTERM = 0.2f;
    #endregion

    #region 플레이어 공격 관련
    public const string PLAYERATTACKNAME_DEFAULT = "DefaultAttack";

    // 방어 처리 가능한 각도
    public const float PLAYER_BLOCKABLE_ANGLE = 140.0f;

    // Perfect Guard 텀 시간
    public const float PROTECT_GUARD_TERMTIME = 0.2f;
    #endregion

    #region UI 관련
    // 적 상태 UI 가 표시될 시간
    public const float ENEMY_STATEUI_VISIBLE_TIMESECONDS = 5.0f;
    #endregion

    #region 오브젝트 레이어
    public static int LAYER_PLAYERCHARACTER => LayerMask.GetMask("PlayerCharacter");

    public static int LAYER_ENEMYCHARACTER => LayerMask.GetMask("EnemyCharacter");
    #endregion

    #region 오브젝트 태그
    public const string TAG_PLAYER = "Player";
    #endregion
}

