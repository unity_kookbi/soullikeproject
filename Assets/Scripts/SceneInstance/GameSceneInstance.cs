using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneInstance : SceneInstanceBase
{
    [Header("# 게임에 사용될 카메라 컴포넌트")]
    public Camera m_UseCamera;

    [Header("# 게임 UI")]
    public GameUI m_GameUI;
}
